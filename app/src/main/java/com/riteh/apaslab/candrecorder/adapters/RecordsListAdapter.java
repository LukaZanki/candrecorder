package com.riteh.apaslab.candrecorder.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.riteh.apaslab.candrecorder.R;
import com.riteh.apaslab.candrecorder.objectclasses.FileDetails;

import java.util.List;


public class RecordsListAdapter extends ArrayAdapter<FileDetails> {

	private List<FileDetails> items;
	private int layoutResourceId;
	private Context context;

	public RecordsListAdapter(Context context, int layoutResourceId, List<FileDetails> items) {
		super(context, layoutResourceId, items);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.items = items;
	}
	
	@SuppressLint("ViewHolder")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		RecordHolder holder = null;

		LayoutInflater inflater = ((android.app.Activity) context).getLayoutInflater();
		row = inflater.inflate(layoutResourceId, parent, false);

		holder = new RecordHolder();
		holder.record = items.get(position);
		
		if( holder.record.getIs_uploaded() > 0){
			holder.uploadRecord = (ImageButton)row.findViewById(R.id.record_uploaded);
			row.findViewById(R.id.record_upload).setVisibility(View.GONE);
			holder.uploadRecord.setVisibility(View.VISIBLE);
			holder.uploadRecord.setTag(holder.record);
		} else {
			holder.uploadRecord = (ImageButton)row.findViewById(R.id.record_upload);
			holder.uploadRecord.setTag(holder.record);
		}
		
		
		
		holder.infoRecord = (ImageButton)row.findViewById(R.id.record_info);
		holder.infoRecord.setTag(holder.record);
		
		holder.deleteRecord = (ImageButton)row.findViewById(R.id.record_delete);
		holder.deleteRecord.setTag(holder.record);

		holder.name = (TextView)row.findViewById(R.id.record_name);

		row.setTag(holder);

		setupItem(holder);
		return row;
	}
	
	private void setupItem(RecordHolder holder) {
		holder.name.setText(holder.record.getName());
	}
	
	public static class RecordHolder {
		FileDetails record;
		TextView name;
		ImageButton uploadRecord;
		ImageButton infoRecord;
		ImageButton deleteRecord;
	}
}
