package com.riteh.apaslab.candrecorder.objectclasses;

import android.content.Context;
import android.widget.ImageView;

import com.riteh.apaslab.candrecorder.R;

public class AssignIcons {
    public static void setIcon(Context context,ImageView imageView, String name){
        if(name.equals(context.getString(R.string.walking)))
            imageView.setImageResource(R.drawable.walkingregular);
        if(name.equals(context.getString(R.string.running)))
            imageView.setImageResource(R.drawable.running);
        if(name.equals(context.getString(R.string.driving_car)))
            imageView.setImageResource(R.drawable.car);
        if(name.equals(context.getString(R.string.tram_train)))
            imageView.setImageResource(R.drawable.train);
        if(name.equals(context.getString(R.string.driving_bicycle)))
            imageView.setImageResource(R.drawable.cycling);

        if(name.equals(context.getString(R.string.hand)))
            imageView.setImageResource(R.drawable.handmobile);
        if(name.equals(context.getString(R.string.pocket)))
            imageView.setImageResource(R.drawable.pocket);
        if(name.equals(context.getString(R.string.bag_backpack)))
            imageView.setImageResource(R.drawable.bag);
        if(name.equals(context.getString(R.string.mounted_fixed)))
            imageView.setImageResource(R.drawable.mobilefix);

        if(name.equals(context.getString(R.string.asphalt)))
            imageView.setImageResource(R.drawable.walkingasfalt);
        if(name.equals(context.getString(R.string.off_road)))
            imageView.setImageResource(R.drawable.makadamwalking);
        if(name.equals(context.getString(R.string.stairs)))
            imageView.setImageResource(R.drawable.step);

        if(name.equals(context.getString(R.string.smooth)))
            imageView.setImageResource(R.drawable.smooth);
        if(name.equals(context.getString(R.string.dynamic)))
            imageView.setImageResource(R.drawable.dinamic);
        if(name.equals(context.getString(R.string.zig_zag)))
            imageView.setImageResource(R.drawable.zig);
        if(name.equals(context.getString(R.string.normal)))
            imageView.setImageResource(R.drawable.normal);


    }
}
