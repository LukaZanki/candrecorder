package com.riteh.apaslab.candrecorder.objectclasses;

import android.graphics.drawable.Drawable;

public class SensorInfo {
    public Drawable icon;
    public String sensorName;

    public SensorInfo(){
        super();
    }

    public SensorInfo(Drawable icon, String sensorName){
        super();
        this.icon = icon;
        this.sensorName = sensorName;
    }


}
