package com.riteh.apaslab.candrecorder;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

public class MainActivity extends Activity {

	// Splash screen timer
	private static int SPLASH_TIME_OUT = 2000;

	private SharedPreferences shared_pref;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		shared_pref = getSharedPreferences(getString(R.string.app_name), MODE_PRIVATE);

		if ( shared_pref.contains("cb_splash") ){
			//ako je cb_state = true
			if( shared_pref.getBoolean("cb_splash", false) ){
				Intent i = new Intent(MainActivity.this, StartActivity.class);
				startActivity(i);
				finish();
			} else {
				new Handler().postDelayed(new Runnable() {
					/*
					 * Showing splash screen with a timer. This will be useful when you
					 * want to show case your app logo / company
					 */

					@Override
					public void run() {
						// This method will be executed once the timer is over
						// Start your app main activity
						Intent i = new Intent(MainActivity.this, StartActivity.class);
						startActivity(i);
						finish();

						// close this activity

						finish();
					}
				}, SPLASH_TIME_OUT);
			}
		} else {
			new Handler().postDelayed(new Runnable() {
				/*
				 * Showing splash screen with a timer. This will be useful when you
				 * want to show case your app logo / company
				 */

				@Override
				public void run() {
					// This method will be executed once the timer is over
					// Start your app main activity
					Intent i = new Intent(MainActivity.this, StartActivity.class);
					startActivity(i);
					finish();

					// close this activity
					finish();
				}
			}, SPLASH_TIME_OUT);

		}


	}

}
