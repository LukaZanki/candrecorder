/*
 * Copyright (C) 2013 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.riteh.apaslab.candrecorder;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;

import com.riteh.apaslab.candrecorder.adapters.PageListAdapter;
import com.riteh.apaslab.candrecorder.common.view.SlidingTabLayout;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SlidingTabsBasicFragment extends Fragment {
    private Map<String, List<String>> menuCollection;
    private List<String> groups;
    private ProgressBar progress;
    private static HashMap<String,Integer> imgMap;


    public List<String> getGroups() {
        return groups;
    }

    public void setGroups(List<String> groups) {
        this.groups = groups;
    }

    public Map<String, List<String>> getMenuCollection() {
        return menuCollection;
    }

    public void setMenuCollection(Map<String, List<String>> menuCollection) {
        this.menuCollection = menuCollection;
    }


    private SlidingTabLayout mSlidingTabLayout;

    private ViewPager mViewPager;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_sample, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        mViewPager = (ViewPager) view.findViewById(R.id.viewpager);
        mViewPager.setAdapter(new SamplePagerAdapter(menuCollection,groups));

        mSlidingTabLayout = (SlidingTabLayout) view.findViewById(R.id.sliding_tabs);
        mSlidingTabLayout.setViewPager(mViewPager);
    }

    public void setProgress(ProgressBar progress) {
        this.progress = progress;
    }

    class SamplePagerAdapter extends PagerAdapter {
        private Map<String, List<String>> menuCollection;
        private List<String> groups;
        public SamplePagerAdapter(Map<String, List<String>> menuCollection, List<String> groups){
            this.menuCollection=menuCollection;
            this.groups=groups;
        }
        @Override
        public int getCount() {
            return groups.size();
        }

        @Override
        public boolean isViewFromObject(View view, Object o) {
            return o == view;
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return groups.get(position);
        }

        @Override
        public Object instantiateItem(ViewGroup container, int position) {
            final int groupIdex=position;
            // Inflate a new layout from our resources
            View view = getActivity().getLayoutInflater().inflate(R.layout.pager_item,
                    container, false);
            // Add the newly created View to the ViewPager
            container.addView(view);

            // Retrieve a ListView from the inflated View, and update it's text
            final ListView listView = (ListView) view.findViewById(R.id.pager_list);
            final PageListAdapter listAdapter = new PageListAdapter(
                    container.getContext(), R.layout.pager_list_item, menuCollection.get(this.groups.get(position)),position);
            listView.setAdapter(listAdapter);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    if(view.isActivated()|| view.isSelected()) {
                        if(groupIdex!=0 && groupIdex!=1) {
                            view.setSelected(false);
                            view.setActivated(false);
                            AssignActivity.checked_items.remove(groupIdex);
                            AssignActivity.addProgress();
                        }
                    }
                    else {
                        view.setSelected(true);
                        view.setActivated(true);
                        if(AssignActivity.checked_items.containsKey(groupIdex))
                            AssignActivity.checked_items.remove(groupIdex);
                        AssignActivity.checked_items.put(groupIdex,position);
                        AssignActivity.addProgress();
                    }

                    mViewPager.refreshDrawableState();
                    listAdapter.notifyDataSetChanged();
                }


            });
            // Return the View
            return view;
        }


        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

    }
}
