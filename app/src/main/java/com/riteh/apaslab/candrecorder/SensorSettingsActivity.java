package com.riteh.apaslab.candrecorder;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.riteh.apaslab.candrecorder.adapters.SensorSettingsAdapter;
import com.riteh.apaslab.candrecorder.objectclasses.SensorInfo;

import java.util.ArrayList;

public class SensorSettingsActivity extends Activity {

	private SensorManager mSensorManager;
	SensorSettingsAdapter adapter;
	SensorInfo sensors_avail[];
	LocationManager manager = null;
	Context context;
	@Override
	protected void onCreate(Bundle savedInstanceState){
		super.onCreate(savedInstanceState);
		context = this;
		setContentView(R.layout.activity_sensor_settings);
		manager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
		final ListView listApplication = (ListView)findViewById(R.id.sensorListview);
		ArrayList<String> listOfChecked = new ArrayList<String>();
		Button b= (Button) findViewById(R.id.save_settings);
		b.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) {

				SharedPreferences.Editor editor = getSharedPreferences(getString(R.string.app_name) + "_sensors", MODE_PRIVATE).edit();
				editor.clear().commit();
				for (int i = 0; i < sensors_avail.length; i++) {
					if (adapter.mCheckStates.get(i) == true) {
						editor.putBoolean(sensors_avail[i].sensorName, true);
						if (sensors_avail[i].sensorName.equals(R.string.gps)) {
							if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
								editor.putBoolean("GPS", false);
							}
						} else {
							editor.putBoolean(sensors_avail[i].sensorName, false);
						}
					} else {
						editor.putBoolean("GPS", true);
					}

					editor.putBoolean("exists", true);
					editor.commit();
					Toast.makeText(SensorSettingsActivity.this, getString(R.string.sensor_saved), Toast.LENGTH_SHORT).show();
					finish();
				}
			}

		});


		// --------- DATA FEED ---------- //

		sensors_avail = detect_sensors();

		SharedPreferences shared_pref = getSharedPreferences(getString(R.string.app_name), MODE_PRIVATE);

		for(SensorInfo item : sensors_avail){
			if( shared_pref.getBoolean(item.sensorName, false) == true){
				listOfChecked.add(item.sensorName);
			} else {
				listOfChecked.remove(item.sensorName);
			}
		}

		adapter = new SensorSettingsAdapter(this, R.layout.sensor_item_row, sensors_avail, listOfChecked);

		listApplication.setAdapter(adapter);
	}

	private SensorInfo[] detect_sensors(){
		/** Check all sensors **/
		ArrayList<SensorInfo> sensors = new ArrayList<SensorInfo>();

		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		if (mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null){
			sensors.add(new SensorInfo(getResources().getDrawable(R.drawable.ic_edit), 
					getString(R.string.accelerometer)));
		} else {
			// Failure! No accelerometer sensor.
			//REQUIRED: Print a NULL value to file or somewhere else.
		}

		if (mSensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE) != null){
			//Success! There is a accelerometer.
			sensors.add(new SensorInfo(getResources().getDrawable(R.drawable.ic_edit), 
					getString(R.string.ambient_temp)));
		} else {
			// Failure! No ambient temperature sensor.
			//REQUIRED: Print a NULL value to file or somewhere else.
			System.out.println("No Temperature Sensor");
		}

		if (mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY) != null){
			//Success! There is a accelerometer.
			sensors.add(new SensorInfo(getResources().getDrawable(R.drawable.ic_edit), 
					getString(R.string.gravity)));
		} else {
			// Failure! No gravity sensor.
			//REQUIRED: Print a NULL value to file or somewhere else.
			System.out.println("No Gravity Sensor");
		}

		if (mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE) != null){
			//Success! There is a accelerometer.
			sensors.add(new SensorInfo(getResources().getDrawable(R.drawable.ic_edit), 
					getString(R.string.gyroscope)));
		} else {
			// Failure! No gravity sensor.
			//REQUIRED: Print a NULL value to file or somewhere else.
			System.out.println("No Gyroscope");
		}

		if (mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT) != null){
			//Success! There is a accelerometer.
			sensors.add(new SensorInfo(getResources().getDrawable(R.drawable.ic_edit), 
					getString(R.string.light)));
		} else {
			// Failure! No gravity sensor.
			//REQUIRED: Print a NULL value to file or somewhere else.
			System.out.println("No Light Sensor");
		}

		if (mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION) != null){
			//Success! There is a accelerometer.
			sensors.add(new SensorInfo(getResources().getDrawable(R.drawable.ic_edit), 
					getString(R.string.lin_acceleration)));
		} else {
			// Failure! No gravity sensor.
			//REQUIRED: Print a NULL value to file or somewhere else.
			System.out.println("No Linear Acceleration Sensor");
		}

		if (mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD) != null){
			//Success! There is a accelerometer.
			sensors.add(new SensorInfo(getResources().getDrawable(R.drawable.ic_edit), 
					getString(R.string.magnetic_field)));
		} else {
			// Failure! No gravity sensor.
			//REQUIRED: Print a NULL value to file or somewhere else.
			System.out.println("No Magnetic Field Sensor");
		}

		if (mSensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE) != null){
			//Success! There is a accelerometer.
			sensors.add(new SensorInfo(getResources().getDrawable(R.drawable.ic_edit), 
					getString(R.string.pressure)));
		} else {
			// Failure! No gravity sensor.
			//REQUIRED: Print a NULL value to file or somewhere else.
			System.out.println("No Pressure Sensor");
		}

		if (mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY) != null){
			//Success! There is a accelerometer.
			sensors.add(new SensorInfo(getResources().getDrawable(R.drawable.ic_edit), 
					getString(R.string.proximity)));
		} else {
			// Failure! No gravity sensor.
			//REQUIRED: Print a NULL value to file or somewhere else.
			System.out.println("No Proximity Sensor");
		}

		if (mSensorManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY) != null){
			//Success! There is a accelerometer.
			sensors.add(new SensorInfo(getResources().getDrawable(R.drawable.ic_edit), 
					getString(R.string.humidity)));
		} else {
			// Failure! No gravity sensor.
			//REQUIRED: Print a NULL value to file or somewhere else.
			System.out.println("No Relative Humidity Sensor");
		}
		if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
			AlertDialog.Builder builder = new AlertDialog.Builder(context);
			builder.setMessage(
					"Your GPS module is disabled. Would you like to enable it ?")
					.setCancelable(false)
					.setPositiveButton("Yes",
							new DialogInterface.OnClickListener() {

								public void onClick(DialogInterface dialog,
													int id) {

									startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));

									dialog.dismiss();
								}
							})
					.setNegativeButton("No",
							new DialogInterface.OnClickListener() {

								public void onClick(DialogInterface dialog,
													int id) {
									dialog.cancel();
								}
							});

			AlertDialog alert = builder.create();
			alert.show();
		} else {
			// Failure! GPS not enabled.
			//REQUIRED: Print a NULL value to file or somewhere else.
			System.out.println("GPS is enabled");
		}
		sensors.add(new SensorInfo(getResources().getDrawable(R.drawable.ic_edit),
				getString(R.string.gps)));

			SensorInfo[] s = new SensorInfo[sensors.size()];
			for (int i = 0; i < sensors.size(); i++) {
				s[i] = sensors.get(i);
			}
			return s;
		}

}
