package com.riteh.apaslab.candrecorder;

import android.animation.ObjectAnimator;
import android.animation.ValueAnimator;
import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.DecelerateInterpolator;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.riteh.apaslab.candrecorder.objectclasses.GroupList;
import com.riteh.apaslab.candrecorder.sql.DataSource;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

@SuppressLint("UseSparseArrays")
public class AssignActivity extends FragmentActivity {

	private Menu menu;
	public static final int PROGRESSUNIT=25;
	List<String> groupList;
	List<String> childList;
	ArrayList<String[]> children = new ArrayList<String[]>();
	Map<String, List<String>> menuCollection;
	Editable value;
	FragmentTransaction transaction;
	private ArrayList<Integer> invisible;
	public static Map<Integer, Integer> checked_items = new HashMap<Integer, Integer>();

	public static ProgressBar progress;
	public static TextView progressText;

	private DataSource datasource = null;
	private com.riteh.apaslab.candrecorder.objectclasses.Activity item = null;
	boolean mode;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_assign_sliding);
		datasource = new DataSource(this);
		datasource.open();
		checked_items = new HashMap<Integer, Integer>();
		createGroupList();
		createCollection();
		mode = getIntent().getBooleanExtra("mode", false);
		invisible = new ArrayList<Integer>();
		transaction = getSupportFragmentManager().beginTransaction();
		SlidingTabsBasicFragment fragment = new SlidingTabsBasicFragment();
		fragment.setGroups(groupList);
		fragment.setMenuCollection(menuCollection);
		fragment.setProgress(progress);
		datasource.open();
		if (!mode) {
			findViewById(R.id.edit_activity).setVisibility(View.GONE);
			findViewById(R.id.save_activity).setVisibility(View.VISIBLE);
		} else {
			findViewById(R.id.save_activity).setVisibility(View.GONE);
			findViewById(R.id.edit_activity).setVisibility(View.VISIBLE);

			long id = getIntent().getLongExtra("id", 0);
			item = datasource.getActivityWithTypeName(id);

			if (!item.getAct_type().equals("")) {
				checked_items.put(0, Arrays.asList(children.get(0)).indexOf(item.getAct_type()));
				invisible.add(0);
			}

			if (!item.getSensor_place().equals("")) {
				checked_items.put(1, Arrays.asList(children.get(1)).indexOf(item.getSensor_place()));
				invisible.add(1);
			}

			if (!item.getSurface_type().equals("")) {
				checked_items.put(2, Arrays.asList(children.get(2)).indexOf(item.getSurface_type()));
				invisible.add(2);
			}

			if (!item.getStyle().equals("")) {
				checked_items.put(3, Arrays.asList(children.get(3)).indexOf(item.getStyle()));
				invisible.add(3);
			}

		}
		transaction.replace(R.id.sample_content_fragment, fragment);
		transaction.commit();
		datasource.close();
	}

	private boolean checkSelection(){
		if( checked_items.containsKey(0) && checked_items.containsKey(1) ){
			return true;
		}
		return false;
	}

	public void btnPressed(View v){
		switch(v.getId()){
		case R.id.edit_activity:
		case R.id.save_activity:

			if( progress.getProgress() < 2 && !checkSelection() ){
				AlertDialog.Builder builder = new AlertDialog.Builder(this);
				builder.setMessage(getString(R.string.not_completed)+"!")
				.setCancelable(false)
				.setPositiveButton(getString(R.string.dialog_ok), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int id) {

					}
				});
				AlertDialog alert = builder.create();
				alert.show();
			} else {



				AlertDialog.Builder diaBox = AskOption(null);
				diaBox.show();
			}
			break;
		case R.id.reset_activity:
			checked_items.clear();
			invisible.clear();
			addProgress(0);
			break;
		default:
			return;
		}
	}

	private AlertDialog.Builder AskOption(String warning)
	{
		AlertDialog.Builder alert = new AlertDialog.Builder(this);


		if(warning == null){
			alert.setMessage(getString(R.string.dialog_want_to_save_activity));
		} else {
			alert.setMessage(warning + "\n" + getString(R.string.dialog_want_to_save_activity));
		}

		final EditText input = new EditText(this);
		if(mode){ //EDIT
			alert.setTitle(getString(R.string.dialog_title_edit));
			alert.setIcon(R.drawable.save);

			value = null;
			value = input.getText();
			if( checked_items.containsKey(0) )
				value.append(children.get(0)[checked_items.get(0)].charAt(0));
			if( checked_items.containsKey(1) )
				value.append(children.get(1)[checked_items.get(1)].charAt(0));
			if( checked_items.containsKey(2) )
				value.append(children.get(2)[checked_items.get(2)].charAt(0));
			if( checked_items.containsKey(3) )
				value.append(children.get(3)[checked_items.get(3)].charAt(0));
			if( checked_items.containsKey(4) )
				value.append(children.get(4)[checked_items.get(4)].charAt(0));
			if( checked_items.containsKey(5) )
				value.append(children.get(5)[checked_items.get(5)].charAt(0));

			input.setText(value.toString());
			input.setSelectAllOnFocus(true);
			value = input.getText();
		} else { //CREATE
			alert.setTitle(getString(R.string.dialog_title_save));
			alert.setIcon(R.drawable.save);
			value = null;
			value = input.getText();
			if( checked_items.containsKey(0) )
				value.append(children.get(0)[checked_items.get(0)].charAt(0));
			if( checked_items.containsKey(1) )
				value.append(children.get(1)[checked_items.get(1)].charAt(0));
			if( checked_items.containsKey(2) )
				value.append(children.get(2)[checked_items.get(2)].charAt(0));
			if( checked_items.containsKey(3) )
				value.append(children.get(3)[checked_items.get(3)].charAt(0));
			if( checked_items.containsKey(4) )
				value.append(children.get(4)[checked_items.get(4)].charAt(0));
			if( checked_items.containsKey(5) )
				value.append(children.get(5)[checked_items.get(5)].charAt(0));

			input.setText(value.toString());
			input.setSelectAllOnFocus(true);
			value = input.getText();
		}

		alert.setView(input);

		alert.setPositiveButton(getString(R.string.dialog_yes_save), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				datasource.open();
				long activities;
				long sensor_placement;
				long surface_type;
				long style;
				long total_time = 0;

				if (checked_items.containsKey(0)) {
					activities = checked_items.get(0)+1;
				} else {
					activities = 1;
				}
				if (checked_items.containsKey(1)) {
					sensor_placement = checked_items.get(1)+1;
				} else {
					sensor_placement = 1;
				}
				if (checked_items.containsKey(2)) {
					surface_type = checked_items.get(2)+1;
				} else {
					surface_type = 1;
				}
				if (checked_items.containsKey(3)) {
					style = checked_items.get(3)+1;
				} else {
					style = 1;
				}
				if (value.length() > 20) {
					AskOption(getString(R.string.dialog_warning_max_chars)).show();
					return;
				} else if (value.length() == 0) {
					if (mode) { //edit
						datasource.editActivity(item.getId(), value.toString(), System.currentTimeMillis(), item.getTotal_time(),
								activities, sensor_placement,
								surface_type, style);
					} else { //create
						datasource.createActivity(value.toString(), System.currentTimeMillis(), total_time,
								activities, sensor_placement,
								surface_type, style);
					}
					finish();
				} else {
					if (!mode) { //create
						datasource.createActivity(value.toString(), System.currentTimeMillis(), total_time,
								activities, sensor_placement,
								surface_type, style);
						finish();
					} else { //edit
						datasource.editActivity(item.getId(), value.toString(), System.currentTimeMillis(), item.getTotal_time(),
								activities, sensor_placement,
								surface_type, style);
						finish();
					}
				}
				datasource.close();
			}
		});

		alert.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				// Canceled.
			}
		});

		return alert;
	}

	public static void addProgress(){
		int max= AssignActivity.progress.getMax();
		int currProgress=AssignActivity.checked_items.size();
		ValueAnimator valueAnimator=ValueAnimator.ofInt(currProgress, currProgress*PROGRESSUNIT);
		valueAnimator.setObjectValues();
		ObjectAnimator animation = ObjectAnimator.ofInt(AssignActivity.progress, "progress", currProgress*PROGRESSUNIT);
		animation.setDuration(500); //in milliseconds
		animation.setInterpolator(new DecelerateInterpolator());
		animation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
			@Override
			public void onAnimationUpdate(ValueAnimator animation) {
				AssignActivity.progressText.setText((int) animation.getAnimatedValue() + "%");
			}
		});
		animation.start();
	}
	public static void addProgress(int prgr){
		int max= AssignActivity.progress.getMax();
		int currProgress=AssignActivity.checked_items.size();
		ValueAnimator valueAnimator=ValueAnimator.ofInt(currProgress, prgr);
		valueAnimator.setObjectValues();
		ObjectAnimator animation = ObjectAnimator.ofInt(AssignActivity.progress, "progress", currProgress*PROGRESSUNIT);
		animation.setDuration(500); //in milliseconds
		animation.setInterpolator(new DecelerateInterpolator());
		animation.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
			@Override
			public void onAnimationUpdate(ValueAnimator animation) {
				AssignActivity.progressText.setText((int) animation.getAnimatedValue() + "%");
			}
		});
		animation.start();
	}

	private void createGroupList() {
		groupList = new ArrayList<String>();
		List<GroupList> groupListAll = datasource.getAllGroupList();
		for (int i = 0; i<groupListAll.size();i++) {
			groupList.add(groupListAll.get(i).getName_group_list());
		}
	}

	private void createCollection() {
		String[] activities = datasource.getAllActivityType();
		String[] sensor_placement = datasource.getAllSensorPlacement();
		String[] surface_type = datasource.getAllSurfaceType();
		String[] style = datasource.getAllStyleType();

		children.clear();
		children.add(activities);
		children.add(sensor_placement);
		children.add(surface_type);
		children.add(style);

		menuCollection = new LinkedHashMap<String, List<String>>();

		for (String menu_item : groupList) {
			if (menu_item.equals(getString(R.string.activity))) {
				loadChild(activities);
			} else if (menu_item.equals(getString(R.string.sensor_placement)))
				loadChild(sensor_placement);
			else if (menu_item.equals(getString(R.string.pavement_type)))
				loadChild(surface_type);
			else if (menu_item.equals(getString(R.string.style)))
				loadChild(style);

			menuCollection.put(menu_item, childList);
		}
	}

	private void loadChild(String[] chosen_activites) {
		childList = new ArrayList<String>();
		for (String model : chosen_activites)
			childList.add(model);
	}

	// Convert pixel to dip
	public int getDipsFromPixel(float pixels) {
		// Get the screen's density scale
		final float scale = getResources().getDisplayMetrics().density;
		// Convert the dps to pixels, based on density scale
		return (int) (pixels * scale + 0.5f);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.assign, menu);
		this.menu=menu;
		(menu.findItem(R.id.action_progress)).setActionView(R.layout.actionbar_progress);
		progress=(ProgressBar)menu.findItem(R.id.action_progress).getActionView().findViewById(R.id.actionbar_progresbar);
		progressText = (TextView)menu.findItem(R.id.action_progress).getActionView().findViewById(R.id.progressText);
		if(mode)
			addProgress(0);
		else
			addProgress();
		return true;
	}
	private void adActionProgressBar(){

	}
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch(item.getItemId()){
		case R.id.action_sensors:
			Intent SensorSettingsIntent = new Intent(AssignActivity.this, SensorSettingsActivity.class);
			AssignActivity.this.startActivity(SensorSettingsIntent);
			break;
		case R.id.action_about:
			Toast.makeText(this, "about", Toast.LENGTH_SHORT).show();
			break;
		case R.id.action_settings:
			Intent PreferencesActivityIntent = new Intent(AssignActivity.this, PreferencesActivity.class);
			AssignActivity.this.startActivity(PreferencesActivityIntent);
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onResume() {
		datasource.open();
		super.onResume();
	}

	@Override
	protected void onPause() {
		datasource.close();
		super.onPause();
	}
	@Override
	protected  void onStart() {
		super.onStart();
	}
}