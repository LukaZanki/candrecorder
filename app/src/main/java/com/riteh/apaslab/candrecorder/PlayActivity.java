package com.riteh.apaslab.candrecorder;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.PowerManager;
import android.text.Editable;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;


import com.riteh.apaslab.candrecorder.common.logger.Log;
import com.riteh.apaslab.candrecorder.objectclasses.Activity;
import com.riteh.apaslab.candrecorder.objectclasses.AssignIcons;
import com.riteh.apaslab.candrecorder.objectclasses.FileDetails;
import com.riteh.apaslab.candrecorder.objectclasses.NNT;
import com.riteh.apaslab.candrecorder.objectclasses.PieChart;
import com.riteh.apaslab.candrecorder.sql.DataSource;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class PlayActivity extends android.app.Activity implements SensorEventListener {

	private DataSource datasource = null;
	private Activity item = null;
	private FileDetails record = null;

	private long start;
	private boolean isRecording;

	private SensorManager mSensorManager = null;
	private LocationManager mlocManager = null;
	private LocationListener mlocListener;

	private static final long MIN_TIME_BW_UPDATES = 0;
	private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 0;

	private Sensor accelerometer = null;
	private Sensor ambient_temp = null;
	private Sensor gravity = null;
	private Sensor gyroscope = null;
	private Sensor light = null;
	private Sensor linear_acceleration = null;
	private Sensor magnetic_field = null;
	private Sensor pressure = null;
	private Sensor proximity = null;
	private Sensor relative_humidity = null;

	private FileOutputStream fos_accelerometer = null;
	private FileOutputStream fos_ambient_temp = null;
	private FileOutputStream fos_gravity = null;
	private FileOutputStream fos_gyroscope = null;
	private FileOutputStream fos_light = null;
	private FileOutputStream fos_linear_acceleration = null;
	private FileOutputStream fos_magnetic_field = null;
	private FileOutputStream fos_pressure = null;
	private FileOutputStream fos_proximity = null;
	private FileOutputStream fos_relative_humidity = null;
	private FileOutputStream fos_gps = null;
	private  TextView textCounter,activityDesc,activityName;
	private  Timer timer;
	private MyTimerTask myTimerTask;
	private PieChart pieChart;
	private LinearLayout pieChartView;
	private ImageView activityIcon;
	private NNT nnt;

	float[] gravity_array = new float[3];
	float[] linear_acc = new float[3];
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_play);
		datasource = new DataSource(getApplicationContext());
		datasource.open();
		pieChartView=(LinearLayout)findViewById(R.id.chart);

		boolean prefs_exist = getIntent().getBooleanExtra("prefs_exist", false);
		long id = getIntent().getLongExtra("id", 0);

		item = datasource.getActivityWithTypeName(id);
		datasource.close();
		if(prefs_exist){
			SharedPreferences prefs = getSharedPreferences(getString(R.string.app_name), MODE_PRIVATE);
			setup_sensors(prefs.getAll());
			prefs = null;
		} else {
			setup_sensors(null);
		}

		record = new FileDetails();
		activityIcon=(ImageView)findViewById(R.id.activityIcon);
		AssignIcons.setIcon(this,activityIcon,item.getAct_type());
		textCounter = (TextView)findViewById(R.id.recordingTime);
		activityDesc = (TextView)findViewById(R.id.item_desc);
		activityName = (TextView)findViewById(R.id.item_name);
		activityName.setText(item.getName());
		activityDesc.setText(item.getDescription());
	}

	private void setup_sensors(Map<String, ?> avail_sensors){

		mSensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);
		mlocManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);

		if(avail_sensors != null){
			for (Map.Entry<String, ?> entry : avail_sensors.entrySet()) {
				if(!entry.getKey().equals("exists")){
					if (entry.getKey().equals(getString(R.string.accelerometer)) && entry.getValue().equals(true)){
						accelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
					} else if (entry.getKey().equals(getString(R.string.ambient_temp)) && entry.getValue().equals(true)){
						ambient_temp = mSensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
					} else if (entry.getKey().equals(getString(R.string.gravity)) && entry.getValue().equals(true)){
						gravity = mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
					} else if (entry.getKey().equals(getString(R.string.gyroscope)) && entry.getValue().equals(true)){
						gyroscope = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
					}else if (entry.getKey().equals(getString(R.string.light)) && entry.getValue().equals(true)){
						light = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
					} else if (entry.getKey().equals(getString(R.string.lin_acceleration)) && entry.getValue().equals(true)){
						linear_acceleration = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
					} else if (entry.getKey().equals(getString(R.string.magnetic_field)) && entry.getValue().equals(true)){
						magnetic_field = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
					} else if (entry.getKey().equals(getString(R.string.pressure)) && entry.getValue().equals(true)){
						pressure = mSensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
					} else if (entry.getKey().equals(getString(R.string.proximity)) && entry.getValue().equals(true)){
						proximity = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
					} else if (entry.getKey().equals(getString(R.string.humidity)) && entry.getValue().equals(true)){
						relative_humidity = mSensorManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY);
					} else {
						System.out.println("No sensors in SharedPreferences");
					}
				}
			} 
		} else {
			if (mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null){
				//Success! There is a accelerometer.
				accelerometer = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
			} else {
				// Failure! No accelerometer sensor.
				//REQUIRED: Print a NULL value to file or somewhere else.
				System.out.println("No Accelerometer Sensor");
			}

			if (mSensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE) != null){
				//Success! There is a accelerometer.
				ambient_temp = mSensorManager.getDefaultSensor(Sensor.TYPE_AMBIENT_TEMPERATURE);
			} else {
				// Failure! No ambient temperature sensor.
				//REQUIRED: Print a NULL value to file or somewhere else.
				System.out.println("No Temperature Sensor");
			}

			if (mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY) != null){
				//Success! There is a accelerometer.
				gravity = mSensorManager.getDefaultSensor(Sensor.TYPE_GRAVITY);
			} else {
				// Failure! No gravity sensor.
				//REQUIRED: Print a NULL value to file or somewhere else.
				System.out.println("No Gravity Sensor");
			}

			if (mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE) != null){
				//Success! There is a accelerometer.
				gyroscope = mSensorManager.getDefaultSensor(Sensor.TYPE_GYROSCOPE);
			} else {
				// Failure! No gravity sensor.
				//REQUIRED: Print a NULL value to file or somewhere else.
				System.out.println("No Gyroscope");
			}

			if (mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT) != null){
				//Success! There is a accelerometer.
				light = mSensorManager.getDefaultSensor(Sensor.TYPE_LIGHT);
			} else {
				// Failure! No gravity sensor.
				//REQUIRED: Print a NULL value to file or somewhere else.
				System.out.println("No Light Sensor");
			}

			if (mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION) != null){
				//Success! There is a accelerometer.
				linear_acceleration = mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION);
			} else {
				// Failure! No gravity sensor.
				//REQUIRED: Print a NULL value to file or somewhere else.
				System.out.println("No Linear Acceleration Sensor");
			}

			if (mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD) != null){
				//Success! There is a accelerometer.
				magnetic_field = mSensorManager.getDefaultSensor(Sensor.TYPE_MAGNETIC_FIELD);
			} else {
				// Failure! No gravity sensor.
				//REQUIRED: Print a NULL value to file or somewhere else.
				System.out.println("No Magnetic Field Sensor");
			}

			if (mSensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE) != null){
				//Success! There is a accelerometer.
				pressure = mSensorManager.getDefaultSensor(Sensor.TYPE_PRESSURE);
			} else {
				// Failure! No gravity sensor.
				//REQUIRED: Print a NULL value to file or somewhere else.
				System.out.println("No Pressure Sensor");
			}

			if (mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY) != null){
				//Success! There is a accelerometer.
				proximity = mSensorManager.getDefaultSensor(Sensor.TYPE_PROXIMITY);
			} else {
				// Failure! No gravity sensor.
				//REQUIRED: Print a NULL value to file or somewhere else.
				System.out.println("No Proximity Sensor");
			}

			if (mSensorManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY) != null){
				//Success! There is a accelerometer.
				relative_humidity = mSensorManager.getDefaultSensor(Sensor.TYPE_RELATIVE_HUMIDITY);
			} else {
				// Failure! No gravity sensor.
				//REQUIRED: Print a NULL value to file or somewhere else.
				System.out.println("No Relative Humidity Sensor");
			}
		}
	}

	public void onClick(View v){
		mlocManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
		PowerManager powerManager = (PowerManager)getSystemService(POWER_SERVICE);
		PowerManager.WakeLock wakeLock = powerManager.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, "MyWakeLock");
		switch(v.getId()){
		case R.id.start_recording:
			wakeLock.acquire();
			nnt=new NNT(this);
			pieChartView.removeAllViewsInLayout();
			nnt.onResume();
			StringBuilder sb = new StringBuilder();
			do{
				sb.delete(0, sb.length());
				Random r = new Random();
				while(sb.length() < 4){
					sb.append(String.valueOf(r.nextInt(9) + 1));
				}
			} while (datasource.sessionExists(sb.toString()));
			record.setSession_id(Integer.parseInt(sb.toString()));

			if(accelerometer != null){
				mSensorManager.registerListener(this, accelerometer, SensorManager.SENSOR_DELAY_NORMAL);
				record.setAccelerometer(buildFilename("accelerometer"));
				try {
					fos_accelerometer = new FileOutputStream(getRecordsStorageDir(buildFilename("accelerometer")));
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} 
			}
			if(ambient_temp != null){
				mSensorManager.registerListener(this, ambient_temp, SensorManager.SENSOR_DELAY_NORMAL);
				record.setAmbientTemp(buildFilename("ambientTemp"));
				try {
					fos_ambient_temp = new FileOutputStream(getRecordsStorageDir(buildFilename("ambientTemp")));
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} 
			}
			if(gravity != null){
				mSensorManager.registerListener(this, gravity, SensorManager.SENSOR_DELAY_NORMAL);
				record.setGravity(buildFilename("gravity"));
				try {
					fos_gravity = new FileOutputStream(getRecordsStorageDir(buildFilename("gravity")));
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			}
			if(gyroscope  != null){
				mSensorManager.registerListener(this, gyroscope , SensorManager.SENSOR_DELAY_NORMAL);
				record.setGyroscope(buildFilename("gyroscope"));
				try {
					fos_gyroscope = new FileOutputStream(getRecordsStorageDir(buildFilename("gyroscope")));
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} 
			}
			if(light  != null){
				mSensorManager.registerListener(this, light , SensorManager.SENSOR_DELAY_NORMAL);
				record.setLight(buildFilename("light"));
				try {
					fos_light = new FileOutputStream(getRecordsStorageDir(buildFilename("light")));
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} 
			}
			if(linear_acceleration  != null){
				mSensorManager.registerListener(this, linear_acceleration , SensorManager.SENSOR_DELAY_NORMAL);
				record.setLinearAccel(buildFilename("linearAccel"));
				try {
					fos_linear_acceleration = new FileOutputStream(getRecordsStorageDir(buildFilename("linearAccel")));
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				} 
			}
			if(magnetic_field  != null){
				mSensorManager.registerListener(this, magnetic_field , SensorManager.SENSOR_DELAY_NORMAL);
				record.setMagneticField(buildFilename("magneticField"));
				try {
					fos_magnetic_field = new FileOutputStream(getRecordsStorageDir(buildFilename("magneticField")));
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			}
			if(pressure  != null){
				mSensorManager.registerListener(this, pressure , SensorManager.SENSOR_DELAY_NORMAL);
				record.setPressure(buildFilename("pressure"));
				try {
					fos_pressure = new FileOutputStream(getRecordsStorageDir(buildFilename("pressure")));
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			}
			if(proximity  != null){
				mSensorManager.registerListener(this, proximity , SensorManager.SENSOR_DELAY_NORMAL);
				record.setProximity(buildFilename("proximity"));
				try {
					fos_proximity = new FileOutputStream(getRecordsStorageDir(buildFilename("proximity")));
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			}
			if(relative_humidity  != null){
				mSensorManager.registerListener(this, relative_humidity, SensorManager.SENSOR_DELAY_NORMAL);
				record.setRelativeHumidity(buildFilename("relativeHumidity"));
				try {
					fos_relative_humidity = new FileOutputStream(getRecordsStorageDir(buildFilename("relativeHumidity")));
				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
			}
			if (mlocManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
				record.setGPS(buildFilename("GPSLocation"));
				try {
					fos_gps = new FileOutputStream(getRecordsStorageDir(buildFilename("GPSLocation")));

				} catch (FileNotFoundException e) {
					e.printStackTrace();
				}
				mlocListener = new LocationListener() {
					@Override
					public void onLocationChanged(Location loc) {
						long currentTime = System.currentTimeMillis();
						String latitude = "" +loc.getLatitude();
						String longitude = "" + loc.getLongitude();
						String altitude = "" + loc.getAltitude();
						String accuracy = "" + loc.getAccuracy();
						String speed = "" + loc.getSpeed();
						String bearing = "" + loc.getBearing();
						try {
							fos_gps.write((currentTime+","+latitude+","+longitude + ","+altitude+","+accuracy+","+speed+","+bearing+"\n").getBytes());
							fos_gps.flush();
						} catch (IOException e) {
							e.printStackTrace();
						}
					}

					@Override
					public void onStatusChanged(String provider, int status, Bundle extras) {

					}

					@Override
					public void onProviderEnabled(String provider) {

					}

					@Override
					public void onProviderDisabled(String provider) {

					}
				};
				mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, MIN_TIME_BW_UPDATES, MIN_DISTANCE_CHANGE_FOR_UPDATES , mlocListener);
		}

		findViewById(R.id.start_recording).setVisibility(View.GONE);
			findViewById(R.id.stop_recording).setVisibility(View.VISIBLE);

			start = System.currentTimeMillis();
			showTimer();
			isRecording = true;
			break;
		case R.id.stop_recording:
			isRecording = false;
			findViewById(R.id.start_recording).setVisibility(View.VISIBLE);
			findViewById(R.id.stop_recording).setVisibility(View.GONE);
			record.setDuration(System.currentTimeMillis() - start);
			pieChart=new PieChart(this,pieChartView,nnt.getChartData());
			nnt.onPause();
			nnt.onReset();
			mSensorManager.unregisterListener(this);
			try {
				if(mlocManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
					mlocManager.removeUpdates(mlocListener);
				}
			}catch(Exception e){
				e.printStackTrace();
			}
			mlocListener = null;

			if(fos_accelerometer != null){
				try {
					fos_accelerometer.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(fos_ambient_temp != null){
				try {
					fos_ambient_temp.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(fos_gravity != null){
				try {
					fos_gravity.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(fos_gyroscope != null){
				try {
					fos_gyroscope.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(fos_light != null){
				try {
					fos_light.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(fos_linear_acceleration != null){
				try {
					fos_linear_acceleration.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(fos_magnetic_field != null){
				try {
					fos_magnetic_field.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(fos_pressure != null){
				try {
					fos_pressure.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(fos_proximity != null){
				try {
					fos_proximity.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(fos_relative_humidity != null){
				try {
					fos_relative_humidity.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			if(fos_gps != null){
				try {
					fos_gps.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

			AlertDialog.Builder diaBox = AskOption(null);
			if (timer!=null){
				timer.cancel();
				timer = null;
			}
			diaBox.show();
			isRecording = false;
			if (wakeLock.isHeld()) {
				wakeLock.release();
			}
			break;
		default: 
			break;
		}
	}

	private AlertDialog.Builder AskOption(String warning)
	{
		AlertDialog.Builder alert = new AlertDialog.Builder(this);

		if(warning == null){
			alert.setMessage(getString(R.string.dialog_want_to_save_record));
		} else {
			alert.setMessage(warning + "\n" + getString(R.string.dialog_want_to_save_record));
		}

		Calendar c = Calendar.getInstance();
		SimpleDateFormat df = new SimpleDateFormat("HHmmss", Locale.getDefault());
		String formattedDate = df.format(c.getTime());

		final EditText input = new EditText(this);
		input.setText(item.getName()+"-"+formattedDate);
		input.setSelectAllOnFocus(true);

		alert.setTitle(getString(R.string.dialog_title_save));
		alert.setIcon(R.drawable.ic_save);

		alert.setView(input);

		alert.setPositiveButton(getString(R.string.dialog_yes_save), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
				Editable value = null;
				value = input.getText();
				if(value.length() > 20){
					AskOption(getString(R.string.dialog_warning_max_chars)).show();
					return;
				}else if(value.length() == 0){
					value.append(item.getName());
					datasource.createRecord(value.toString(), System.currentTimeMillis(), record.getDuration(),
							record.getSession_id(), 0,
							record.getAccelerometer(), record.getAmbientTemp(), record.getGravity(), record.getGyroscope(),
							record.getLight(), record.getLinearAccel(), record.getMagneticField(),
							record.getPressure(), record.getProximity(), record.getRelativeHumidity(), record.getGPS(), item.getId());
					datasource.setTotalTimeActivity(item.getId(), record.getDuration());
					Toast.makeText(PlayActivity.this, getString(R.string.dialog_successfully_saved) +
                            getString(R.string.app_name) + "/" + record.getSession_id() + ")", Toast.LENGTH_LONG).show();
				} else {
					datasource.createRecord(value.toString(), System.currentTimeMillis(), record.getDuration(),
							record.getSession_id(), 0,
							record.getAccelerometer(), record.getAmbientTemp(), record.getGravity(), record.getGyroscope(),
							record.getLight(), record.getLinearAccel(), record.getMagneticField(),
							record.getPressure(), record.getProximity(), record.getRelativeHumidity(), record.getGPS(), item.getId());
					datasource.setTotalTimeActivity(item.getId(), record.getDuration());
					Toast.makeText(PlayActivity.this, getString(R.string.dialog_successfully_saved) +
                            getString(R.string.app_name) + "/" + record.getSession_id() + ")", Toast.LENGTH_LONG).show();
				}
			}
		});

		alert.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {

				if(PlayActivity.isSDWritable()){
					File sdCard = Environment.getExternalStorageDirectory();
					File directory = new File(sdCard.getAbsolutePath()+ "/"
							+ getString(R.string.app_name)+"/"
							+ record.getSession_id());
					if (directory.isDirectory()) {
						String[] children = directory.list();
						for (int i = 0; i < children.length; i++) {
							new File(directory, children[i]).delete();
						}
						directory.delete();
					}
				}
				Toast.makeText(PlayActivity.this, getString(R.string.dialog_no), Toast.LENGTH_LONG).show();
			}
		});

		return alert;
	}

	@Override
	public void onSensorChanged(SensorEvent event) {
		if (isRecording){
			Sensor sensor = event.sensor;
			if (sensor.getType() == Sensor.TYPE_ACCELEROMETER) {
				final float alpha = (float) 0.8;

				// Isolate the force of gravity with the low-pass filter.
				gravity_array[0] = alpha * gravity_array[0] + (1 - alpha) * event.values[0];
				gravity_array[1] = alpha * gravity_array[1] + (1 - alpha) * event.values[1];
				gravity_array[2] = alpha * gravity_array[2] + (1 - alpha) * event.values[2];

				// Remove the gravity contribution with the high-pass filter.
				linear_acc[0] = event.values[0] - gravity_array[0];
				linear_acc[1] = event.values[1] - gravity_array[1];
				linear_acc[2] = event.values[2] - gravity_array[2];

				try {
					fos_accelerometer.write((System.currentTimeMillis()+","+linear_acc[0]+","+linear_acc[1]+","+linear_acc[2]+"\n").getBytes());
					fos_accelerometer.flush();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else if (sensor.getType() == Sensor.TYPE_AMBIENT_TEMPERATURE){
				try {
					fos_ambient_temp.write((System.currentTimeMillis()+","+event.values[0]+"\n").getBytes());
					fos_ambient_temp.flush();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else if (sensor.getType() == Sensor.TYPE_GRAVITY){
				try {
					fos_gravity.write((System.currentTimeMillis()+","+event.values[0]+","+event.values[1]+","+event.values[2]+"\n").getBytes());
					fos_gravity.flush();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else if (sensor.getType() == Sensor.TYPE_GYROSCOPE){
				try {
					fos_gyroscope.write((System.currentTimeMillis()+","+event.values[0]+","+event.values[1]+","+event.values[2]+"\n").getBytes());
					fos_gyroscope.flush();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else if (sensor.getType() == Sensor.TYPE_LIGHT){
				try {
					fos_light.write((System.currentTimeMillis()+","+event.values[0]+"\n").getBytes());
					fos_light.flush();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else if (sensor.getType() == Sensor.TYPE_LINEAR_ACCELERATION){
				try {
					fos_linear_acceleration.write((System.currentTimeMillis()+","+event.values[0]+","+event.values[1]+","+event.values[2]+"\n").getBytes());
					fos_linear_acceleration.flush();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else if (sensor.getType() == Sensor.TYPE_MAGNETIC_FIELD){
				try {
					fos_magnetic_field.write((System.currentTimeMillis()+","+event.values[0]+","+event.values[1]+","+event.values[2]+"\n").getBytes());
					fos_magnetic_field.flush();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else if (sensor.getType() == Sensor.TYPE_PRESSURE){
				try {
					fos_pressure.write((System.currentTimeMillis()+","+event.values[0]+"\n").getBytes());
					fos_pressure.flush();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else if (sensor.getType() == Sensor.TYPE_PROXIMITY){
				try {
					fos_proximity.write((System.currentTimeMillis()+","+event.values[0]+"\n").getBytes());
					fos_proximity.flush();
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else if (sensor.getType() == Sensor.TYPE_RELATIVE_HUMIDITY){
				try {
					fos_relative_humidity.write((System.currentTimeMillis()+","+event.values[0]+"\n").getBytes());
					fos_relative_humidity.flush();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}

	}

	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {

	}

	private String buildFilename(String type){

		StringBuilder sb = new StringBuilder();

		if( !item.getAct_type().equals("") ){
			sb.append(item.getAct_type().charAt(0));
		} else {
			sb.append("X");
		}
		if( !item.getSensor_place().equals("") ){
			sb.append(item.getSensor_place().charAt(0));
		} else {
			sb.append("X");
		}

		if( !item.getSurface_type().equals("") ){
			sb.append(item.getSurface_type().charAt(0));
		} else {
			sb.append("X");
		}

		if( !item.getStyle().equals("") ){
			sb.append(item.getStyle().charAt(0));
		} else {
			sb.append("X");
		}

		sb.append("_");
		sb.append(type);
		sb.append("_");
		sb.append(Activity.getDate(System.currentTimeMillis(), "ddMMyy"));
		sb.append("_");
		sb.append(record.getSession_id());
		sb.append(".csv");
		return sb.toString();
	}

	public static boolean isSDWritable() {
		String state = Environment.getExternalStorageState();
		return Environment.MEDIA_MOUNTED.equals(state);
	}

	private File getRecordsStorageDir(String recordings_file_name) {
		if(isSDWritable()){
			File sdCard = Environment.getExternalStorageDirectory();
			File directory = new File(sdCard.getAbsolutePath()+ "/"
					+ getString(R.string.app_name)+"/"
					+ record.getSession_id());
			directory.mkdirs();
			return new File(directory, recordings_file_name);
		} else return null;
	}

	@Override
	protected void onResume() {
		super.onResume();
		datasource.open();
	}

	@Override
	protected void onPause() {
		super.onPause();
		datasource.close();
	}
private void showTimer(){
	if(timer != null){
		timer.cancel();
	}

	timer = new Timer();
	myTimerTask = new MyTimerTask();

		//delay 1000ms, repeat in 5000ms
		timer.schedule(myTimerTask, 0, 1000);

}
	class MyTimerTask extends TimerTask {
		SimpleDateFormat simpleDateFormat =
				new SimpleDateFormat("HH:mm:ss");


		@Override
		public void run() {

			final long time =(System.currentTimeMillis()-start)/1000;
			runOnUiThread(new Runnable(){

				@Override
				public void run() {
					textCounter.setText(String.format("%02d:%02d:%02d",  time / 3600,time / 60, time% 60));
				}});
		}

	}

}
