package com.riteh.apaslab.candrecorder.objectclasses;


public class FileDetails {
	private long id;
	private String name;
	private long timestamp;
	private long duration;
	private int session_id;
	private int is_uploaded;
	private String accelerometer = null;
	private String ambientTemp = null;
	private String gravity = null;
	private String gyroscope = null;
	private String light = null;
	private String linearAccel = null;
	private String magneticField = null;
	private String pressure = null;
	private String proximity = null;
	private String relativeHumidity = null;
	private String GPS = null;
	private long ID_Activity;
	private Activity activity = null;

	public Activity getActivity() {
		return activity;
	}

	public void setActivity(Activity activity) {
		this.activity = activity;
	}

	public long getId() {
		return id;
	}

	public String getGPS() {
		return GPS;
	}

	public void setGPS(String GPS) {
		this.GPS = GPS;
	}

	public long getID_Activity() {
		return ID_Activity;
	}

	public void setID_Activity(long ID_Activity) {
		this.ID_Activity = ID_Activity;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getSession_id() {
		return session_id;
	}
	
	public int getIs_uploaded() {
		return is_uploaded;
	}

	public void setIs_uploaded(int is_uploaded) {
		this.is_uploaded = is_uploaded;
	}

	public void setSession_id(int session_id) {
		this.session_id = session_id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public long getDuration() {
		return duration;
	}

	public void setDuration(long duration) {
		this.duration = duration;
	}

	public String getAccelerometer() {
		return accelerometer;
	}

	public void setAccelerometer(String accelerometer) {
		this.accelerometer = accelerometer;
	}

	public String getAmbientTemp() {
		return ambientTemp;
	}

	public void setAmbientTemp(String ambient_temp) {
		this.ambientTemp = ambient_temp;
	}

	public String getGravity() {
		return gravity;
	}

	public void setGravity(String gravity) {
		this.gravity = gravity;
	}

	public String getGyroscope() {
		return gyroscope;
	}

	public void setGyroscope(String gyroscope) {
		this.gyroscope = gyroscope;
	}

	public String getLight() {
		return light;
	}

	public void setLight(String light) {
		this.light = light;
	}

	public String getLinearAccel() {
		return linearAccel;
	}

	public void setLinearAccel(String linear_acc) {
		this.linearAccel = linear_acc;
	}

	public String getMagneticField() {
		return magneticField;
	}

	public void setMagneticField(String magnetic_field) {
		this.magneticField = magnetic_field;
	}

	public String getPressure() {
		return pressure;
	}

	public void setPressure(String pressure) {
		this.pressure = pressure;
	}

	public String getProximity() {
		return proximity;
	}

	public void setProximity(String proximity) {
		this.proximity = proximity;
	}

	public String getRelativeHumidity() {
		return relativeHumidity;
	}

	public void setRelativeHumidity(String relative_humidity) {
		this.relativeHumidity = relative_humidity;
	}

	public String getDescription() {
		return "Last modified: "+Activity.getDate(timestamp, "dd/MM/yyyy HH:mm:ss")
				+ " Duration (mm:ss:SSS): "+Activity.getDate(duration, "mm:ss:SSS")
				+ " Session _id: "+session_id
				+ " Is uploaded: "+(is_uploaded == 1 ? "true" : "false");
	}
}
