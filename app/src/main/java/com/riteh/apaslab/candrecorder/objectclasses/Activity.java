package com.riteh.apaslab.candrecorder.objectclasses;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

public class Activity {
	private long id;
	private String name;
	private long timestamp;
	private long total_time;
	private long id_act_type;
	private long id_sensor;
	private long id_surface;
	private long id_style;
	private String act_type;
	private String sensor_place;
	private String surface_type;
	private String style;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getId_act_type() {
		return id_act_type;
	}

	public long getId_sensor() {
		return id_sensor;
	}

	public long getId_surface() {
		return id_surface;
	}

	public long getId_style() {
		return id_style;
	}

	public String getAct_type() {
		return act_type;
	}

	public void setAct_type(String act_type) {
		this.act_type = act_type;
	}

	public String getSensor_place() {
		return sensor_place;
	}

	public void setSensor_place(String sensor_place) {
		this.sensor_place = sensor_place;
	}

	public String getSurface_type() {
		return surface_type;
	}

	public void setSurface_type(String surface_type) {
		this.surface_type = surface_type;
	}

	public String getStyle() {
		return style;
	}

	public void setStyle(String style) {
		this.style = style;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public long getTotal_time() {
		return total_time;
	}

	public void setTotal_time(long total_time) {
		this.total_time = total_time;
	}


	public void setId_act_type(long id_act_type) {
		this.id_act_type = id_act_type;
	}


	public void setId_sensor(long id_sensor) {
		this.id_sensor = id_sensor;
	}


	public void setId_surface(long id_surface) {
		this.id_surface = id_surface;
	}


	public void setId_style(long id_style) {
		this.id_style = id_style;
	}

	@Override
	public String toString() {
		return "Name: "+name+"\n"
				+ "Timestamp: "+timestamp+"\n"
				+ "Total Time: "+total_time+"\n"
				+ "Activity type: "+act_type+"\n"
				+ "Sensor placement: "+sensor_place+"\n"
				+ "Surface type: "+surface_type+"\n"
				+ "Style: "+style+"\n";
	}
	
	public static String getDate(long milliSeconds, String dateFormat)
	{
	    // Create a DateFormatter object for displaying date in specified format.
	    SimpleDateFormat formatter = new SimpleDateFormat(dateFormat, Locale.ENGLISH);

	    // Create a calendar object that will convert the date and time value in milliseconds to date. 
	     Calendar calendar = Calendar.getInstance();
	     calendar.setTimeInMillis(milliSeconds);
	     return formatter.format(calendar.getTime());
	}
	public String getDescription() {
		String temp="";
		if(!style.equals(""))
			temp+=style+" ";
		temp+=act_type+" with the phone";

		if(!("Mounted/Fixed to vehicle".equals(sensor_place)))
			temp+=" in the";
		temp+=" "+sensor_place;
		if(!surface_type.equals(""))
			temp+=" on "+surface_type + "\nDuration: "+ getTotalTimeFormated();
		return temp;
	}

	public String getTotalTimeFormated(){
		String hms = String.format("%02d:%02d:%02d", TimeUnit.MILLISECONDS.toHours(getTotal_time()),
				TimeUnit.MILLISECONDS.toMinutes(total_time) - TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(total_time)),
				TimeUnit.MILLISECONDS.toSeconds(total_time) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(total_time)));
		return hms;
	}

	public String getNameForItem(){
		String temp = "";
		temp+= getName() + " - " + getTotal_time();
		return temp;
	}
	
}
