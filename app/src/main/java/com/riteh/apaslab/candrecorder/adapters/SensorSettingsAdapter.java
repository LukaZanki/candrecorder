package com.riteh.apaslab.candrecorder.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.SparseBooleanArray;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.riteh.apaslab.candrecorder.objectclasses.SensorInfo;
import com.riteh.apaslab.candrecorder.R;

import java.util.ArrayList;

public class SensorSettingsAdapter extends ArrayAdapter<SensorInfo> implements CompoundButton.OnCheckedChangeListener {
	public SparseBooleanArray mCheckStates = new SparseBooleanArray();

	Context context;
	int layoutResourceId;
	SensorInfo  data[] = null;
	ArrayList<String> listOfChecked;

	public SensorSettingsAdapter(Context context, int layoutResourceId, SensorInfo[] data, ArrayList<String> listOfChecked){
		super(context, layoutResourceId,data);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.data = data;
		this.listOfChecked = listOfChecked;
		mCheckStates = new SparseBooleanArray(data.length+1);
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent){

		View row = convertView;
		SensorInfoHolder holder= null;

		if (row == null){

			LayoutInflater inflater = ((Activity)context).getLayoutInflater();
			row = inflater.inflate(layoutResourceId, parent, false);

			holder = new SensorInfoHolder();

			holder.imgIcon = (ImageView) row.findViewById(R.id.sensor_icon);
			holder.txtTitle = (TextView) row.findViewById(R.id.sensor_name);
			holder.chkSelect = (CheckBox) row.findViewById(R.id.sensor_checkbox);
			
			row.setTag(holder);

		}
		else{
			holder = (SensorInfoHolder)row.getTag();
		}

		SensorInfo appinfo = data[position];
		holder.txtTitle.setText(appinfo.sensorName);
		holder.imgIcon.setImageDrawable(appinfo.icon);
		holder.chkSelect.setTag(position);
		
		if(!listOfChecked.isEmpty()){
			if( listOfChecked.contains(appinfo.sensorName) ){
				holder.chkSelect.toggle();
				mCheckStates.put(position, mCheckStates.get(position, true));
			} else {
				holder.chkSelect.setChecked(mCheckStates.get(position, false));
				mCheckStates.delete(position);
			}
		} else {
			holder.chkSelect.setChecked(mCheckStates.get(position, true));
			mCheckStates.put(position, mCheckStates.get(position, true));
		}
		
		holder.chkSelect.setOnCheckedChangeListener(this);
		return row;

	}
	public boolean isChecked(int position) {
		return mCheckStates.get(position, false);
	}

	public void setChecked(int position, boolean isChecked) {
		mCheckStates.put(position, isChecked);
	}

	public void toggle(int position) {
		setChecked(position, !isChecked(position));

	}
	@Override
	public void onCheckedChanged(CompoundButton buttonView,
			boolean isChecked) {

		mCheckStates.put((Integer) buttonView.getTag(), isChecked);

	}
	static class SensorInfoHolder
	{
		ImageView imgIcon;
		TextView txtTitle;
		CheckBox chkSelect;

	}
}

