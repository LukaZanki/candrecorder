package com.riteh.apaslab.candrecorder.sql;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SQLiteHelper extends SQLiteOpenHelper {

	public static final String [] ACTIVITY_TYPE = {"Walking", "Running", "Car Driving", "Bicycle Riding", "Tram/Train"};

	public static final String [] SENSOR_PLACEMENT = {"Hand", "Pocket", "Bag/Backpack", "Mounted/Fixed to vehicle"};

	public static final String [] SURFACE_TYPE = {"Asphalt/Concrete/Other types of smooth flat terrain", "Off Road/Macadam/Other types of flat but rough terrain", "Stairs"};

	public static final String [] STYLE_TYPE = {"Normal", "Dynamic", "Zig/Zagging", "Smooth"};

	public static final String [] GROUP_LIST_NAMES = {"* Activity", "* Sensor Placement", "Surface Type", "Style"};
	public static final String [] GROUP_LIST_TABLE_NAMES = {"activities", "activity_type", "sensor_placement", "surface_type"};

	public static final String TABLE_NAME_GROUP_LIST = "group_list";
	public static final String COLUMN_ID_GROUP_LIST = "id_group_list";
	public static final String COLUMN_NAME_GL = "name";
	public static final String COLUMN_TABLE_NAME = "table_name";

	public static final String TABLE_NAME_ACTIVITY = "activities";
	public static final String COLUMN_ID_ACT = "id_act";
	public static final String COLUMN_NAME_ACT = "name";
	public static final String COLUMN_TIMESTAMP = "time_stamp";
	public static final String COLUMN_TOTAL_TIME = "total_time";

	public static final String TABLE_NAME_ACTIVITY_TYPE = "activity_type";
	public static final String COLUMN_ID_ACT_TYPE = "id_act_type";
	public static final String COLUMN_TYPE_ACT = "type";

	public static final String TABLE_NAME_SENSOR_PLACEMENT = "sensor_placement";
	public static final String COLUMN_ID_SENSOR = "id_sensor";
	public static final String COLUMN_TYPE_SEN = "type";

	public static final String TABLE_NAME_SURFACE_TYPE = "surface_type";
	public static final String COLUMN_ID_SURFACE = "id_surface";
	public static final String COLUMN_TYPE_SURF = "type";

	public static final String TABLE_NAME_STYLE = "style";
	public static final String COLUMN_ID_STYLE = "id_style";
	public static final String COLUMN_TYPE_STYLE = "type";

	public static final String TABLE_NAME_DELETED_ACTIVITIES = "deleted_activities";
	public static final String COLUMN_ID_DELETED_ACT = "id_deleted_act";

	public static final String TABLE_NAME_RECORDING = "recording_for_files";
	public static final String COLUMN_ID_REC = "id_rec";
	public static final String COLUMN_NAME_REC = "name_rec";
	public static final String COLUMN_DURATION = "duration";
	public static final String COLUMN_SESSION_ID = "session_id";
	public static final String COLUMN_IS_UPLOADED = "is_uploaded";
	public static final String COLUMN_ACCELEROMETER = "accelerometer";
	public static final String COLUMN_AMBIENT_TEMP = "ambient_temp";
	public static final String COLUMN_GRAVITY = "gravity";
	public static final String COLUMN_GYROSCOPE = "gyroscope";
	public static final String COLUMN_LIGHT = "light";
	public static final String COLUMN_LINEAR_ACC = "linear_acc";
	public static final String COLUMN_MAGNETIC_FIELD = "magnetic_field";
	public static final String COLUMN_PRESSURE = "pressure";
	public static final String COLUMN_PROXIMITY = "proximity";
	public static final String COLUMN_RELATIVE_HUMIDITY = "relative_humidity";
	public static final String COLUMN_GPS = "gps";

	private static final String DATABASE_NAME = "sensor_gathering.db";
	private static final int DATABASE_VERSION = 15;

	//Group list table creation sql statement
	private static final String CREATE_TABLE_GROUP_LIST = "create table "
			+ TABLE_NAME_GROUP_LIST + "("
			+ COLUMN_ID_GROUP_LIST + " integer primary key autoincrement, "
			+ COLUMN_NAME_GL + " text not null, "
			+ COLUMN_TABLE_NAME + " text not null);";

	//Activity type table creation sql statement
	private static final String CREATE_TABLE_ACTIVITY_TYPE = "create table "
			+ TABLE_NAME_ACTIVITY_TYPE + "("
			+ COLUMN_ID_ACT_TYPE + " integer primary key autoincrement, "
			+ COLUMN_TYPE_ACT + " text not null);";

	//Sensor placement table creation sql statement
	private static final String CREATE_TABLE_SENSOR_PLACEMENT = "create table "
			+ TABLE_NAME_SENSOR_PLACEMENT + "("
			+ COLUMN_ID_SENSOR + " integer primary key autoincrement, "
			+ COLUMN_TYPE_SEN + " text not null);";

	//Surface type table creation sql statement
	private static final String CREATE_TABLE_SURFACE_TYPE = "create table "
			+ TABLE_NAME_SURFACE_TYPE + "("
			+ COLUMN_ID_SURFACE + " integer primary key autoincrement, "
			+ COLUMN_TYPE_SURF + " text not null);";

	//Style table creation sql statement
	private static final String CREATE_TABLE_STYLE = "create table "
			+ TABLE_NAME_STYLE + "("
			+ COLUMN_ID_STYLE + " integer primary key autoincrement, "
			+ COLUMN_TYPE_STYLE + " text not null);";

	public static final String CREATE_TABLE_DELETED_ACT = "create table "
			+ TABLE_NAME_DELETED_ACTIVITIES + "("
			+ COLUMN_ID_DELETED_ACT + " integer primary key autoincrement, "
			+ COLUMN_ID_ACT + " integer);";

	public static final String CREATE_TABLE_ACTIVITY = "create table "
			+ TABLE_NAME_ACTIVITY + "("
			+ COLUMN_ID_ACT + " integer primary key autoincrement, "
			+ COLUMN_NAME_ACT + " text not null, "
			+ COLUMN_TIMESTAMP + " integer not null, "
			+ COLUMN_TOTAL_TIME + " integer, "
			+ COLUMN_ID_ACT_TYPE + " integer not null, "
			+ COLUMN_ID_SENSOR + " integer not null, "
			+ COLUMN_ID_SURFACE + " integer, "
			+ COLUMN_ID_STYLE + " integer);";

	// Recording table creation sql statement
	private static final String CREATE_TABLE_RECORDING = "create table "
			+ TABLE_NAME_RECORDING + "("
			+ COLUMN_ID_REC + " integer primary key autoincrement, "
			+ COLUMN_NAME_REC + " text, "
			+ COLUMN_TIMESTAMP + " integer, "
			+ COLUMN_DURATION + " integer, "
			+ COLUMN_SESSION_ID + " integer, "
			+ COLUMN_IS_UPLOADED + " integer, "
			+ COLUMN_ACCELEROMETER + " text, "
			+ COLUMN_AMBIENT_TEMP + " text, "
			+ COLUMN_GRAVITY + " text, "
			+ COLUMN_GYROSCOPE + " text, "
			+ COLUMN_LIGHT + " text, "
			+ COLUMN_LINEAR_ACC + " text, "
			+ COLUMN_MAGNETIC_FIELD + " text, "
			+ COLUMN_PRESSURE + " text, "
			+ COLUMN_PROXIMITY + " text, "
			+ COLUMN_RELATIVE_HUMIDITY + " text, "
			+ COLUMN_GPS + " text, "
			+ COLUMN_ID_ACT + " integer);";

	public SQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		try {
			db.execSQL(CREATE_TABLE_GROUP_LIST);
			db.execSQL(CREATE_TABLE_ACTIVITY_TYPE);
			db.execSQL(CREATE_TABLE_SENSOR_PLACEMENT);
			db.execSQL(CREATE_TABLE_SURFACE_TYPE);
			db.execSQL(CREATE_TABLE_STYLE);
			db.execSQL(CREATE_TABLE_ACTIVITY);
			db.execSQL(CREATE_TABLE_RECORDING);
			db.execSQL(CREATE_TABLE_DELETED_ACT);
			insertValues(db);
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(SQLiteHelper.class.getName(),
				"Upgrading database from version " + oldVersion + " to "
						+ newVersion + ", which will destroy all old data");
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_RECORDING);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_ACTIVITY);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_GROUP_LIST);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_ACTIVITY_TYPE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_SENSOR_PLACEMENT);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_SURFACE_TYPE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_STYLE);
		db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME_DELETED_ACTIVITIES);
		onCreate(db);
	}

	public void insertValues(SQLiteDatabase db){
		//INSERT GROUP LIST
		for (int i = 0; i<GROUP_LIST_NAMES.length;i++){
			createGroupList(db, GROUP_LIST_NAMES[i], GROUP_LIST_TABLE_NAMES[i]);
		}
		//INSERT ACTIVITY TYPE
		for (int i = 0; i<ACTIVITY_TYPE.length;i++){
			createActivityType(db, ACTIVITY_TYPE[i]);
		}
		//INSERT SENSOR PLACEMENT
		for (int i = 0; i<SENSOR_PLACEMENT.length;i++){
			createSensorPlacement(db, SENSOR_PLACEMENT[i]);
		}
		//INSERT SURFACE TYPE
		for (int i = 0; i<SURFACE_TYPE.length;i++){
			createSurfaceType(db, SURFACE_TYPE[i]);
		}
		//INSERT STYLE TYPE
		for (int i = 0; i<STYLE_TYPE.length;i++){
			createStyle(db,STYLE_TYPE[i]);
		}
	}

	public void createGroupList(SQLiteDatabase db, String name, String table_name) {
		db.execSQL("INSERT INTO " + TABLE_NAME_GROUP_LIST + "(" + COLUMN_NAME_GL + ", " + COLUMN_TABLE_NAME + ") VALUES('" + name + "', '"+ table_name + "');");
	}

	public void createActivityType(SQLiteDatabase db, String name) {
		db.execSQL("INSERT INTO " + TABLE_NAME_ACTIVITY_TYPE + "(" + COLUMN_TYPE_ACT + ") VALUES('" + name + "');");
	}

	public void createSensorPlacement(SQLiteDatabase db, String name) {
		db.execSQL("INSERT INTO " + TABLE_NAME_SENSOR_PLACEMENT + "(" + COLUMN_TYPE_SEN + ") VALUES('" + name + "');");
	}

	public void createSurfaceType(SQLiteDatabase db, String name) {
		db.execSQL("INSERT INTO " + TABLE_NAME_SURFACE_TYPE + "(" + COLUMN_TYPE_SURF + ") VALUES('" + name + "');");
	}

	public void createStyle(SQLiteDatabase db, String name) {
		db.execSQL("INSERT INTO " + TABLE_NAME_STYLE + "(" + COLUMN_TYPE_STYLE + ") VALUES('" + name + "');");
	}

}
