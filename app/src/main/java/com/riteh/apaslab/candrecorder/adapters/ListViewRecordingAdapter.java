package com.riteh.apaslab.candrecorder.adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.daimajia.swipe.SimpleSwipeListener;
import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.BaseSwipeAdapter;
import com.riteh.apaslab.candrecorder.R;
import com.riteh.apaslab.candrecorder.objectclasses.AssignIcons;
import com.riteh.apaslab.candrecorder.objectclasses.FileDetails;

import java.util.List;

public class ListViewRecordingAdapter extends BaseSwipeAdapter {
    private Context mContext;
    private List<FileDetails> items;
    //private HintAnimator slideHint;
    private float slideProgress=0;
    private int bgColor;
    public ListViewRecordingAdapter(Context mContext, List<FileDetails> items) {

        this.mContext = mContext;
        this.items = items;

        //slideHint=new HintAnimator(mContext);
    }


    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swiperecording;
    }

    @Override
    public View generateView(final int position, final ViewGroup parent) {
        View v = LayoutInflater.from(mContext).inflate(R.layout.listviewrecording_item, null);
        final SwipeLayout swipeLayout = (SwipeLayout)v.findViewById(getSwipeLayoutResourceId(position));
        swipeLayout.setShowMode(SwipeLayout.ShowMode.LayDown);
        swipeLayout.addDrag(SwipeLayout.DragEdge.Left, swipeLayout.findViewById(R.id.bottom_wrapper));
        swipeLayout.setRightSwipeEnabled(false);
        swipeLayout.addSwipeListener(new SimpleSwipeListener() {
            @Override
            public void onOpen(SwipeLayout layout) {
                //YoYo.with(Techniques.Tada).duration(500).delay(100).playOn(layout.findViewById(R.id.delete));
                //slideHint.hide();

            }
            @Override
            public void onClose(SwipeLayout layout) {
                //slideHint.hide();
                (layout.findViewById(R.id.frontIcon)).setAlpha(1);
            }

            @Override
            public void onStartOpen(SwipeLayout layout) {
                super.onStartOpen(layout);
                (layout.findViewById(R.id.frontIcon)).setAlpha(0);
            }

            @Override
            public void onUpdate(SwipeLayout layout, int leftOffset, int topOffset) {
                super.onUpdate(layout, leftOffset, topOffset);
                slideProgress=(float)leftOffset /layout.getWidth();
                if(slideProgress<1 && slideProgress>0) {
                    bgColor = blendColors(0xffffffff, 0xffdbdbdb, slideProgress);
                    (layout.findViewById(R.id.recording_upload)).setAlpha(0.5f + slideProgress / 2);
                    (layout.findViewById(R.id.recording_delete)).setAlpha(0.5f + slideProgress / 2);
                    (layout.findViewById(R.id.recording_upload)).setBackgroundColor(bgColor);
                    (layout.findViewById(R.id.recording_delete)).setBackgroundColor(bgColor);
                    (layout.findViewById(R.id.bottom_wrapper)).setBackgroundColor(bgColor);
                }
            }
        });

        swipeLayout.setOnDoubleClickListener(new SwipeLayout.DoubleClickListener() {
            @Override
            public void onDoubleClick(SwipeLayout layout, boolean surface) {
               // Toast.makeText(mContext, "DoubleClick", Toast.LENGTH_SHORT).show();
            }
        });
        swipeLayout.setOnClickListener(new SwipeLayout.OnClickListener() {
            @Override
            public void onClick(View v) {
                swipeLayout.open(SwipeLayout.DragEdge.Left);
                //slideHint.show(v);

            }

        });

        return v;
    }

    @Override
    public void fillValues(int position, View convertView) {
        TextView desc = (TextView)convertView.findViewById(R.id.text_data);
        desc.setText(items.get(position).getDescription());
        RecordingHolder holder;
        ImageView imageView =(ImageView)convertView.findViewById(R.id.activityIcon);
        ImageView imageViewFront =(ImageView)convertView.findViewById(R.id.activityIconFront);
        holder = new RecordingHolder();
        holder.fileDetails = items.get(position);

        holder.deleteRecording = (Button)convertView.findViewById(R.id.recording_delete);
        holder.deleteRecording.setTag(holder.fileDetails);

        holder.uploadRecording = (Button)convertView.findViewById(R.id.recording_upload);
        holder.uploadRecording.setTag(holder.fileDetails);

        holder.name = (TextView)convertView.findViewById(R.id.recording_name);

        AssignIcons.setIcon(mContext, imageView, items.get(position).getActivity().getAct_type());
        AssignIcons.setIcon(mContext, imageViewFront, items.get(position).getActivity().getAct_type());
        holder.imageViewFront = imageView;
        holder.imageView = imageView;
        holder.imageView.setTag(holder.fileDetails);
        holder.imageViewFront.setTag(holder.fileDetails);
        convertView.setTag(holder);

        setupItem(holder);

    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    private void setupItem(RecordingHolder holder) {
        holder.name.setText(holder.fileDetails.getName());
    }

    public static class RecordingHolder {
        FileDetails fileDetails;
        TextView name;
        Button uploadRecording;
        Button deleteRecording;
        ImageView imageView;
        ImageView imageViewFront;
    }
    private static int blendColors(int color1, int color2, float ratio) {
        final float inverseRation = 1f - ratio;
        float r = (Color.red(color1) * ratio) + (Color.red(color2) * inverseRation);
        float g = (Color.green(color1) * ratio) + (Color.green(color2) * inverseRation);
        float b = (Color.blue(color1) * ratio) + (Color.blue(color2) * inverseRation);
        return Color.rgb((int) r, (int) g, (int) b);
    }

}
