package com.riteh.apaslab.candrecorder;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ListView;

import com.daimajia.swipe.util.Attributes;
import com.riteh.apaslab.candrecorder.adapters.ListViewRecordingAdapter;
import com.riteh.apaslab.candrecorder.adapters.MyListViewAdapter;
import com.riteh.apaslab.candrecorder.objectclasses.Activity;
import com.riteh.apaslab.candrecorder.objectclasses.FileDetails;
import com.riteh.apaslab.candrecorder.sql.DataSource;

import java.util.List;

public class StartActivity extends android.app.Activity {

	private DataSource datasource = null;
	List<Activity> values;
	List<FileDetails> valuesRec;
	private ListView mListView;
	private MyListViewAdapter adapter;
	private ListViewRecordingAdapter adapterRec;
	private Context mContext = this;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_start);
		mListView = (ListView) findViewById(R.id.listview);
		datasource = new DataSource(getApplicationContext());
		datasource.open();

		values = datasource.getAllActivityWithTypeName();

		setupListViewAdapter();
		checkValues();
	}

	public void onClickActivity(View v) {
		Activity item = (Activity)v.getTag();
		switch(v.getId()){
		case R.id.activity_play:
			Intent playIntent = new Intent(StartActivity.this, PlayActivity.class);

			SharedPreferences prefs = getSharedPreferences(getString(R.string.app_name), MODE_PRIVATE);
			if(prefs.contains("exists")){
				playIntent.putExtra("prefs_exist", true);
			} else {
				playIntent.putExtra("prefs_exist", false);
			}
			prefs = null;
			playIntent.putExtra("id", item.getId());
			StartActivity.this.startActivity(playIntent);
			break;
		case R.id.activity_details:
			confirmDialog(item);
			break;
		case R.id.activity_edit:
			Intent assignIntent = new Intent(StartActivity.this, AssignActivity.class);
			assignIntent.putExtra("mode", true);
			assignIntent.putExtra("id", item.getId());
			StartActivity.this.startActivity(assignIntent);
			break;
		case R.id.activity_delete:
			AlertDialog diaBox = optionsDialog(item);
			diaBox.show();
			checkValues();
			break;
		default:
			return;
		}
	}

	private void setupListViewAdapter() {

		adapter = new MyListViewAdapter(this,values);
		mListView.setAdapter(adapter);
		adapter.setMode(Attributes.Mode.Single);

		//setup Buttons
		setupButtons();
	}

	private void setupButtons() {
		findViewById(R.id.add_activity).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent myIntent = new Intent(StartActivity.this, AssignActivity.class);
				StartActivity.this.startActivity(myIntent);
			}
		});

		findViewById(R.id.saved_recs).setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Intent myIntent = new Intent(StartActivity.this, RecordingActivity.class);
				StartActivity.this.startActivity(myIntent);
			}
		});
	}

	private void checkValues(){
		if( adapter == null ){
			findViewById(R.id.listview).setVisibility(View.INVISIBLE);
			findViewById(R.id.no_activity_tv).setVisibility(View.VISIBLE);
		} else {
			if(adapter.isEmpty()){
				findViewById(R.id.listview).setVisibility(View.INVISIBLE);
				findViewById(R.id.no_activity_tv).setVisibility(View.VISIBLE);
			} else {
				findViewById(R.id.no_activity_tv).setVisibility(View.INVISIBLE);
				findViewById(R.id.listview).setVisibility(View.VISIBLE);
			}
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch(item.getItemId()){
		case R.id.action_sensors:
			Intent SensorSettingsIntent = new Intent(StartActivity.this, SensorSettingsActivity.class);
			StartActivity.this.startActivity(SensorSettingsIntent);
			break;
		case R.id.action_about:
			Intent AboutActivityIntent = new Intent(StartActivity.this, AboutActivity.class);
			StartActivity.this.startActivity(AboutActivityIntent);
			break;
		case R.id.action_settings:
			Intent PreferencesActivityIntent = new Intent(StartActivity.this, PreferencesActivity.class);
			StartActivity.this.startActivity(PreferencesActivityIntent);
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private AlertDialog optionsDialog(final Activity item){

		AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
		.setTitle(getString(R.string.dialog_title)) 
		.setMessage(getString(R.string.dialog_want_to_delete)+"\n\""+item.getName()+"\"?") 
		.setIcon(R.drawable.ic_delete)

		.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int whichButton) {

				datasource.deleteActivity(item);
				values.remove(item);
				adapter.notifyDataSetChanged();
				dialog.dismiss();
				checkValues();
			}   

		})

		.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {

				dialog.dismiss();

			}
		})
		.create();
		return myQuittingDialogBox;
	}

	private void confirmDialog(final Activity item){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(item.toString())
		.setCancelable(false)
		.setPositiveButton(getString(R.string.dialog_ok), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {

			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

	@Override
	protected void onResume() {
		datasource.open();
		values = datasource.getAllActivityWithTypeName();

		setupListViewAdapter();
		checkValues();
		super.onResume();
	}

	@Override
	protected void onPause() {
		datasource.close();
		super.onPause();
	}
}
