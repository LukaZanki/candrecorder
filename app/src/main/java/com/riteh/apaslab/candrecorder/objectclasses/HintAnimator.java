package com.riteh.apaslab.candrecorder.objectclasses;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.riteh.apaslab.candrecorder.R;

import java.util.Timer;
import java.util.TimerTask;

public class HintAnimator {
    private Timer timer;
    private Animation animation;
    private ImageView arrow1;
    private ImageView arrow2;
    private ImageView arrow3;
    private Context mContext;
    private AnimationTask animationTask;
    private int counter=0;

    public HintAnimator(Context mContext){
        this.mContext=mContext;
        animation = AnimationUtils.loadAnimation(mContext.getApplicationContext(), R.anim.blink);
    }


    public void show(View layout){
        if(timer != null){
            hide();
        }
        /*arrow1=(ImageView)layout.findViewById(R.id.arrow1);
        arrow2=(ImageView)layout.findViewById(R.id.arrow2);
        arrow3=(ImageView)layout.findViewById(R.id.arrow3);
        */
        timer = new Timer();
        animationTask=new AnimationTask();
        timer.schedule(animationTask, 0, 200);

    }
    public void hide() {
        if (timer != null){
            this.arrow1.clearAnimation();
            this.arrow2.clearAnimation();
            this.arrow3.clearAnimation();
            animationTask.cancel();
            timer.cancel();
        }
    }

    class AnimationTask extends TimerTask {

        @Override
        public void run() {
            ((android.app.Activity) mContext).runOnUiThread(new Runnable() {

                @Override
                public void run() {
                    counter++;
                    switch (counter) {
                        case 1:
                            arrow3.clearAnimation();
                            arrow1.startAnimation(animation);
                            break;
                        case 2:
                            arrow1.clearAnimation();
                            arrow2.startAnimation(animation);
                            break;
                        case 3:
                            arrow2.clearAnimation();
                            arrow3.startAnimation(animation);
                            counter = 0;
                            break;

                    }

                }
            });

        }
    }
}
