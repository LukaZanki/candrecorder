package com.riteh.apaslab.candrecorder.adapters;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import com.riteh.apaslab.candrecorder.R;

import java.util.List;
import java.util.Map;


@SuppressLint("InflateParams")
public class ExpandableListAdapter extends BaseExpandableListAdapter {

	private Activity context;
	private Map<String, List<String>> menuCollection;
	private List<String> groups;

	public ExpandableListAdapter(Activity context, List<String> groups,
			Map<String, List<String>> menuCollection) {
		this.context = context;
		this.menuCollection = menuCollection;
		this.groups = groups;
	}

	public Object getChild(int groupPosition, int childPosition) {
		return menuCollection.get(groups.get(groupPosition)).get(childPosition);
	}

	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}


	public View getChildView(final int groupPosition, final int childPosition,
			boolean isLastChild, View convertView, ViewGroup parent) {
		final String child = (String) getChild(groupPosition, childPosition);
		LayoutInflater inflater = context.getLayoutInflater();

		if (convertView == null) {
			convertView = inflater.inflate(R.layout.child_item, null);
		}

		TextView item = (TextView) convertView.findViewById(R.id.child);

		item.setText(child);
		return convertView;
	}

	public int getChildrenCount(int groupPosition) {
		return menuCollection.get(groups.get(groupPosition)).size();
	}

	public Object getGroup(int groupPosition) {
		return groups.get(groupPosition);
	}

	public int getGroupCount() {
		return groups.size();
	}

	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	public View getGroupView(int groupPosition, boolean isExpanded,
			View convertView, ViewGroup parent) {
		String groupName = (String) getGroup(groupPosition);
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.group_item,
					null);
		}

		TextView item = (TextView) convertView.findViewById(R.id.group);
		item.setText(groupName);
		return convertView;
	}

	public boolean hasStableIds() {
		return true;
	}

	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}
}