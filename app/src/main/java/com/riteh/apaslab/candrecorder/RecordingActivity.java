package com.riteh.apaslab.candrecorder;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import com.riteh.apaslab.candrecorder.adapters.ListViewRecordingAdapter;
import com.riteh.apaslab.candrecorder.clientside.HttpPostOperations;
import com.riteh.apaslab.candrecorder.objectclasses.FileDetails;
import com.riteh.apaslab.candrecorder.sql.DataSource;
import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.apache.commons.io.FileUtils;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;

public class RecordingActivity extends android.app.Activity {

	private DataSource datasource = null;
	private ListViewRecordingAdapter adapter = null;
	List<FileDetails> values;
	List<FileDetails> allValues;
	SharedPreferences shared_pref;
	private String key;
	private ProgressDialog barProgressDialog;
	FileDetails item;
	TextView error;
	//GLOBAL FOR SEARCH
	private int choice=1;
	private boolean isSearching = false;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_records);

		datasource = new DataSource(getApplicationContext());
		datasource.open();
		
		barProgressDialog = new ProgressDialog(this);

		values = datasource.getAllRecordings();
		allValues = values;

		shared_pref = getSharedPreferences(getString(R.string.app_name), MODE_PRIVATE);
		error = (TextView) findViewById(R.id.tv_error_records);

		setupListViewAdapter();
		checkValues();
		setupButtons();
	}

	private void setupListViewAdapter() {
		adapter = new ListViewRecordingAdapter(this, values);
		ListView recordListView = (ListView)findViewById(R.id.records_list);
		recordListView.setAdapter(adapter);
	}

	private void setupButtons() {

		if(!isSearching){
			Button resetSearch = (Button)findViewById(R.id.reset_search);
			resetSearch.setEnabled(isSearching);
		}

		findViewById(R.id.delete_all_rec).setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				AlertDialog diaBox = optionsDialog();
				diaBox.show();
				checkValues();
			}
		});

		findViewById(R.id.search).setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				AlertDialog diaBox = searchDialog();
				diaBox.show();

			}
		});

		findViewById(R.id.reset_search).setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				if (isSearching) {
					values = datasource.getAllRecordings();
					setupListViewAdapter();
					checkValues();
					isSearching = false;
					Button resetSearch = (Button) findViewById(R.id.reset_search);
					resetSearch.setEnabled(isSearching);
				}
			}
		});
	}

	public void onClickRecording(View v) {
		item = (FileDetails)v.getTag();
		switch(v.getId()){
		case R.id.recording_upload:
			if( !isFileExists(item) ) {
				error.setVisibility(View.VISIBLE);
				error.setTextColor(Color.RED);
				error.setText(getString(R.string.files_not_found));
				break;
			} else {
				error.setVisibility(View.GONE);
			}
			if( !isRemembered() ){
				Intent loginIntent = new Intent(RecordingActivity.this, HttpPostOperations.class);
				loginIntent.putExtra("session_id", item.getSession_id());
				loginIntent.putExtra("_id", item.getId());
				RecordingActivity.this.startActivity(loginIntent);
				error.setVisibility(View.GONE);
			} else {
				error.setVisibility(View.VISIBLE);
				if( isNetworkConnected() ){
					error.setTextColor(Color.rgb(0, 126, 0));
					error.setText(getString(R.string.internet_avilable));
					startFileUpload();
				} else {
					error.setTextColor(Color.RED);
					error.setText(getString(R.string.internet_unavilable));
				}
			}
			break;
		case R.id.recording_uploaded:
			Toast.makeText(this, getString(R.string.already_uploaded), Toast.LENGTH_SHORT).show();
			break;
		case R.id.recording_delete:
			AlertDialog diaBox = optionsDialog(item);
			diaBox.show();
			checkValues();
			break;
		default:
			return;
		}
	}
	
	private boolean isFileExists(FileDetails item){
		
		File sdCard = Environment.getExternalStorageDirectory();
		File directory = new File(sdCard.getAbsolutePath()+ "/"
				+ getString(R.string.app_name)+"/"
				+ item.getSession_id());
		
		if(directory.exists()) return true;
		
		return false;
	}

	private void startFileUpload(){

		//progress bar 4 showing upload status
		
		barProgressDialog.setTitle("Uploading Files ...");
		barProgressDialog.setMessage("Upload in progress ...");
		barProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		barProgressDialog.setProgress(0);

		final File dir = new File(Environment.getExternalStorageDirectory()+"/"+
				getString(R.string.app_name)+"/"+
				item.getSession_id()+"/");
		traverse(dir);
	}

	private void traverse (File dir) {
		if (dir.exists()) {
			File[] files = dir.listFiles();
			barProgressDialog.setMax(files.length);
			barProgressDialog.show();
			for (int i = 0; i < files.length; ++i) {
				File file = files[i];
				if (file.isDirectory()) {
					traverse(file);
				} else {
					new FileUploadOperation().execute(file.getName(), String.valueOf(i + 1), String.valueOf(files.length));
				}
			}
		}
	} 

	private boolean isRemembered(){
		if ( shared_pref.contains("cb_state") ){
			//ako je cb_state = true
			if( shared_pref.getBoolean("cb_state", false) ){
				key = shared_pref.getString("key", "");
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

	private void checkValues(){
		if( adapter == null ){
			findViewById(R.id.records_list).setVisibility(View.INVISIBLE);
			findViewById(R.id.no_records_tv).setVisibility(View.VISIBLE);
		} else {
			if(adapter.isEmpty()){
				findViewById(R.id.records_list).setVisibility(View.INVISIBLE);
				findViewById(R.id.no_records_tv).setVisibility(View.VISIBLE);
			} else {
				findViewById(R.id.no_records_tv).setVisibility(View.INVISIBLE);
				findViewById(R.id.records_list).setVisibility(View.VISIBLE);
			}
		}
	}

	private AlertDialog optionsDialog(final FileDetails item){

		AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
		.setTitle(getString(R.string.dialog_title)) 
		.setMessage(getString(R.string.dialog_want_to_delete) + "\n\"" + item.getName() + "\"?")
		.setIcon(R.drawable.ic_delete)

		.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {

			public void onClick(DialogInterface dialog, int whichButton) {

				if (PlayActivity.isSDWritable()) {

					File sdCard = Environment.getExternalStorageDirectory();
					File directory = new File(sdCard.getAbsolutePath() + "/"
							+ getString(R.string.app_name) + "/"
							+ item.getSession_id());
					if (directory.isDirectory()) {
						String[] children = directory.list();
						for (int i = 0; i < children.length; i++) {
							new File(directory, children[i]).delete();
						}
						directory.delete();
					}
				}
				datasource.deleteRecording(item.getId(), item.getID_Activity());
				values.remove(item);
				adapter.notifyDataSetChanged();
				dialog.dismiss();
				checkValues();

			}

		})

		.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {

				dialog.dismiss();

			}
		})
		.create();
		return myQuittingDialogBox;
	}

	private AlertDialog optionsDialog(){

		AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
				.setTitle(getString(R.string.dialog_title))
				.setMessage(getString(R.string.dialog_want_to_delete) + " all?")
				.setIcon(R.drawable.ic_delete)

				.setPositiveButton(getString(R.string.dialog_yes), new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int whichButton) {

						if (PlayActivity.isSDWritable()) {

							File sdCard = Environment.getExternalStorageDirectory();
							File directory = new File(sdCard.getAbsolutePath() + "/"
									+ getString(R.string.app_name));
							try {
								FileUtils.cleanDirectory(directory);
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
						datasource.deleteRecordings();
						values.clear();
						adapter.notifyDataSetChanged();
						dialog.dismiss();
						checkValues();
					}

				})

				.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {

						dialog.dismiss();

					}
				})
				.create();
		return myQuittingDialogBox;
	}

	private AlertDialog searchDialog(){
		//Reset search if searching alreadyS
		if (isSearching){
			values = allValues;
		}
		final CharSequence[] items = {""+getString(R.string.item_rec_id), ""+getString(R.string.item_rec_name)};
		final EditText input = new EditText(this);
		input.setHint("Example: RPAN");
		final AlertDialog myQuittingDialogBox =new AlertDialog.Builder(this)
				.setTitle(getString(R.string.dialog_search_title))
				.setIcon(R.drawable.search)
				.setView(input)
				.setSingleChoiceItems(items, 1, new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						setSearchBy(which);
						if (which == 0) {
							input.setHint("Example: 1234");
						} else {
							input.setHint("Example: RPAN");
						}
					}
				})
				.setPositiveButton(getString(R.string.dialog_search_button), new DialogInterface.OnClickListener() {

					public void onClick(DialogInterface dialog, int whichButton) {
						Log.d("INPUT", "INPUT: " + input.getText().toString() + " CHOICE: " + choice);
						String check = checkSearchValues(input.getText().toString());
						choice = 1;
						if (check.equals("")) {
							Log.d("SIZE", "SIZE: " + values.size());
							isSearching = true;
							Button resetSearch = (Button) findViewById(R.id.reset_search);
							resetSearch.setEnabled(isSearching);
							dialog.dismiss();
							setupListViewAdapter();
							checkValues();
						} else {
							AlertDialog showAgain = searchDialog();
							showAgain.show();
							new AlertDialog.Builder(RecordingActivity.this)
									.setTitle("Alert")
									.setMessage(check)
									.setIcon(android.R.drawable.ic_dialog_alert)
									.setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
										public void onClick(DialogInterface dialog, int which) {
											dialog.dismiss();
										}
									}).show();
						}
					}

				})

				.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				})
				.create();
		return myQuittingDialogBox;
	}

	public void setSearchBy(int which){
		choice = which;
	}

	public String checkSearchValues(String input){
		List<FileDetails> tempValues = new ArrayList<FileDetails>();
		if (!input.equals("")) {
			if (choice == 0) {
				try {
					for (int i = 0; i < values.size(); i++) {
						Log.d("SEARCH FOR ID", "SEARCH FOR ID: "+values.get(i).getSession_id()+ " == " + Integer.parseInt(input));
						if (values.get(i).getSession_id() == Integer.parseInt(input)) {
							Log.d("SUCCESS IN SEARCH", "SUCCESS IN SEARCH: " + values.get(i).getSession_id() + " == " + Integer.parseInt(input));
							tempValues.add(values.get(i));
							values.clear();
							values = tempValues;
							return "";
						}
					}
				}catch(Exception e){
					e.printStackTrace();
					return "Integer required!";
				}
			} else if (choice == 1) {
				for (int i = 0; i < values.size(); i++) {
					if (values.get(i).getName().substring(0,4).equals(input) || values.get(i).getName().substring(0,4).equals(input.toUpperCase())) {
						Log.d("NAME", "NAME: "+values.get(i).getName().substring(0,4));
						tempValues.add(values.get(i));
					}
				}
				values.clear();
				values = tempValues;
				return "";
			}
		}
		return "No such file/s";
	}

	private void confirmDialog(final FileDetails item){
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		builder.setMessage(item.toString())
		.setCancelable(false)
		.setPositiveButton(getString(R.string.dialog_ok), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {

			}
		});
		AlertDialog alert = builder.create();
		alert.show();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		switch(item.getItemId()){
		case R.id.action_sensors:
			Intent SensorSettingsIntent = new Intent(RecordingActivity.this, SensorSettingsActivity.class);
			RecordingActivity.this.startActivity(SensorSettingsIntent);
			break;
		case R.id.action_about:
			Intent AboutActivityIntent = new Intent(RecordingActivity.this, AboutActivity.class);
			RecordingActivity.this.startActivity(AboutActivityIntent);
			break;
		case R.id.action_settings:
			Intent PreferencesActivityIntent = new Intent(RecordingActivity.this, PreferencesActivity.class);
			RecordingActivity.this.startActivity(PreferencesActivityIntent);
			break;
		default:
			break;
		}
		return super.onOptionsItemSelected(item);
	}

	private class FileUploadOperation extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {
			String resp;

			HttpClient httpclient = new DefaultHttpClient();
			HttpHost targetHost = new HttpHost("cand.riteh.hr", 80, "http");
			HttpPost httpPost = new HttpPost("/user/file/upload/app/");
			httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);

			String boundary = "-------------" + System.currentTimeMillis();
			httpPost.setHeader("Content-type", "multipart/form-data; boundary="+boundary);

			File myFile = new File(Environment.getExternalStorageDirectory()+"/"+
					getString(R.string.app_name) + "/" +
					params[0].substring(params[0].length()-8, params[0].length()-4)+"/"
					, params[0]);

			//ADD REPLACED ACTIVITIES AS X
			String renamefileName = params[0].substring(0,2) + "XX" + params[0].substring(2, params[0].length());
			Log.d("RENAME", "RENAME: " + renamefileName);
			//RENAME FILE
			myFile.renameTo(new File(Environment.getExternalStorageDirectory()+"/"+
					getString(R.string.app_name) + "/" +
					params[0].substring(params[0].length()-8, params[0].length()-4)+"/" + renamefileName));
			//GET RENAMED FILE
			File renamedFile = new File(Environment.getExternalStorageDirectory()+"/"+
					getString(R.string.app_name) + "/" +
					params[0].substring(params[0].length()-8, params[0].length()-4)+"/" + renamefileName);

			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
			builder.setBoundary(boundary);

			System.out.println(renamedFile.getAbsolutePath());
			ContentBody cbFile = new FileBody(renamedFile);
			builder.addTextBody("key", key);
			System.out.println(key);
			builder.addPart("files", cbFile);
			HttpEntity entity = builder.build();
			httpPost.setEntity(entity);

			try {
				HttpResponse response = httpclient.execute(targetHost,httpPost);
				entity = response.getEntity();
				resp = EntityUtils.toString(entity);

				Log.d("HTTP", "HTTP: OK");
				Log.d("response", resp);

				JSONObject jsonObj = new JSONObject(resp);

				if(jsonObj.getString("success").equals("true")){
					//successfully completed upload
					return params[1]+"-"+params[2];
				} else {
					Log.d("failure:", jsonObj.getString("reason"));
					return jsonObj.getString("reason");
				}
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				Log.e("HTTP", "Error in http connection " + e.toString());
				return getString(R.string.server_not_reachable);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				Log.e("HTTP", "Error in http connection " + e.toString());
				return getString(R.string.server_not_reachable);
			} catch (JSONException e) {
				Log.e("JSON parsing", "Error in parsing JSON object: " + e.toString());
				return getString(R.string.server_not_reachable);
			} finally{
				//RENAME BACK TO ORIGINAL NAME
				renamedFile.renameTo(new File(Environment.getExternalStorageDirectory() + "/" +
						getString(R.string.app_name) + "/" +
						params[0].substring(params[0].length() - 8, params[0].length() - 4) + "/"
						, params[0]));
			}
		}

		@Override
		protected void onPostExecute(String result) {
			if(result.indexOf('-') > 0){ //success
				barProgressDialog.incrementProgressBy(1);
				if (barProgressDialog.getProgress() == barProgressDialog.getMax()) {
					barProgressDialog.dismiss();
				}
				Log.d("RESULT", "RESULT:"+result);
				//check 2 values passed as return value
				if(result.substring(0, result.indexOf('-')).equals(result.substring(result.indexOf('-')+1, result.length()))){
					DataSource.upload_file(item.getId());
					error.setTextColor(Color.rgb(0, 126, 0));
					error.setText(getString(R.string.upload_success));
					Toast.makeText(RecordingActivity.this, getString(R.string.upload_success), Toast.LENGTH_SHORT).show();
					recreate();
				}
			} else {
				barProgressDialog.dismiss();
				error.setTextColor(Color.RED);
				error.setText(result);
			}
		}

		@Override
		protected void onPreExecute() {}

		@Override
		protected void onProgressUpdate(Void... values) {}
	}

	private boolean isNetworkConnected() {
		boolean isConnectedWifi = false;
		boolean isConnectedMobile = false;

		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] netInfo = cm.getAllNetworkInfo();
		for (NetworkInfo ni : netInfo) {
			if (ni.getTypeName().equalsIgnoreCase("WIFI"))
				if (ni.isConnected())
					isConnectedWifi = true;
			if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
				if (ni.isConnected())
					isConnectedMobile = true;
		}
		return isConnectedWifi || isConnectedMobile;
	}

	@Override
	protected void onResume() {
		super.onResume();
		onCreate(null);
	}

	@Override
	protected void onPause() {
		super.onPause();
		datasource.close();
	}
	
	@Override
    protected void onDestroy() {
        super.onDestroy();
        barProgressDialog.dismiss();
    }
}
