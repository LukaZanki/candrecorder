package com.riteh.apaslab.candrecorder.objectclasses;

public class GroupList {
    private long id_group_list;
    private String name_group_list = "";
    private String table_name_group_list = "";

    public long getId_group_list() {
        return id_group_list;
    }

    public void setId_group_list(long id_group_list) {
        this.id_group_list = id_group_list;
    }

    public String getName_group_list() {
        return name_group_list;
    }

    public void setName_group_list(String name_group_list) {
        this.name_group_list = name_group_list;
    }

    public String getTable_name_group_list() {
        return table_name_group_list;
    }

    public void setTable_name_group_list(String table_name_group_list) {
        this.table_name_group_list = table_name_group_list;
    }
}
