package com.riteh.apaslab.candrecorder;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.Toast;

public class PreferencesActivity extends Activity {
	SharedPreferences shared_pref;
	CheckBox cb_login;
	CheckBox cb_splash;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_preferences);

		shared_pref = getSharedPreferences(getString(R.string.app_name), MODE_PRIVATE);

		cb_login = ((CheckBox) findViewById(R.id.cb_login));
		cb_splash = ((CheckBox) findViewById(R.id.cb_splash));

		cb_login.setChecked(shared_pref.getBoolean("cb_state", false));
		cb_splash.setChecked(shared_pref.getBoolean("cb_splash", false));
		

		Button b= (Button) findViewById(R.id.save_settings);
		b.setOnClickListener(new OnClickListener()
		{
			@Override
			public void onClick(View v) {

				SharedPreferences.Editor editor = getSharedPreferences(getString(R.string.app_name), MODE_PRIVATE).edit();

				editor.putBoolean("cb_state", cb_login.isChecked());
				editor.putBoolean("cb_splash", cb_splash.isChecked());
				editor.commit();

				Toast.makeText(PreferencesActivity.this, getString(R.string.sensor_saved), Toast.LENGTH_SHORT).show();
				finish();

			}

		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.preferences, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
