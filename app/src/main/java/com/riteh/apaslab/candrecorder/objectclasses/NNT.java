package com.riteh.apaslab.candrecorder.objectclasses;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.SoundPool;
import android.net.http.AndroidHttpClient;
import android.os.AsyncTask;
import android.util.Log;

import com.riteh.apaslab.candrecorder.R;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.neuroph.core.NeuralNetwork;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Jelena on 19.6.2015..
 */
public class NNT implements SensorEventListener {
    private NeuralNetwork nnet;
    private final int LOADING_DATA_DIALOG = 2;
    //private final String URL="http://activitytracking.byethost18.com/acctivity.php?";
    //HttpClient client;
    private SensorManager senSensorManager;
    private Sensor senAccelerometer;
    private final int TIMEWINDOW = 1000;// miliseconds
    private final double ZC = 0.8;
    private final int INPUTLENGTH = 6;
    private final int OUTPUTLENGTH = 6;
    private final double EVALUATEVALUE = 0.9;
    private double[]currentVector={1,1,1};
    private double min=999,max=0,range,minDegree=360,maxDegree=-360,curDegree;
    private double magA,magB,ans;
    private long lastUpdate = 0, curTime;
    private double x, y, z;
    private double norm = 0, rangeMin = 99, rangeMax = 0, variance = 0, sum = 0;
    private int counter = -1, zeroCross = 0;
    boolean oneTime = true;
    private ArrayList<Double> data = new ArrayList<Double>();
    private double[] input = new double[INPUTLENGTH];
    private double[] output = new double[OUTPUTLENGTH];
    private int lastActivity=0;
    private final String[] activities = {"standing","walking", "running", "cycling","stepUp","stepDown"};
    private boolean networLoadet=false;
    private Dialog dialog;
    private SoundPool soundPool;
    private HashMap<Integer, Integer> soundPoolMap;
    private int soundID = 1;
    private HashMap<String, Integer> chartData;
    Context context;

    public NNT(Context context)

    {
        this.context=context;
        senSensorManager = (SensorManager) context.getSystemService(Context.SENSOR_SERVICE);
        senAccelerometer = senSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
        senSensorManager.registerListener(this, senAccelerometer, SensorManager.SENSOR_DELAY_NORMAL);
        data.add(new Double(0));
        chartData=new HashMap<String, Integer>();
       /* //client = new DefaultHttpClient();
        soundPool = new SoundPool(6, AudioManager.STREAM_MUSIC, 100);
        soundPoolMap = new HashMap<Integer, Integer>();
        soundPoolMap.put(1, soundPool.load(this, R.raw.standing, 1));
        soundPoolMap.put(2, soundPool.load(this, R.raw.walking, 1));
        soundPoolMap.put(3, soundPool.load(this, R.raw.running, 1));
        soundPoolMap.put(4, soundPool.load(this, R.raw.cycling, 1));
        soundPoolMap.put(5, soundPool.load(this, R.raw.stepup, 1));
        soundPoolMap.put(6, soundPool.load(this, R.raw.stepdown, 1));
        */
        loadData();
        initChartData();
    }
    private Runnable loadDataRunnable = new Runnable() {
        public void run() {
            // open neural network
            InputStream is = context.getResources().openRawResource(R.raw.nnt6a);
            // load neural network
            nnet = NeuralNetwork.load(is);
            if(nnet==null)
                Log.d("nnet", "(nnet==null");
            else
                Log.d("nnet", nnet.toString());
            // dismiss loading dialog
            dialog.dismiss();
            networLoadet=true;
        }
    };


    private void loadData() {
        Log.d("loadData", "load");
        dialog = onCreateDialog(LOADING_DATA_DIALOG);
        dialog.show();
        // load neural network in separate thread with stack size = 32000
        new Thread(null,loadDataRunnable, "dataLoader", 64000).start();
    }

    protected Dialog onCreateDialog(int id) {
        ProgressDialog dialog;
        if (id == LOADING_DATA_DIALOG) {
            dialog = new ProgressDialog(context);
            dialog.setTitle("Loading");
            dialog.setMessage("Loading data...");
            dialog.setCancelable(false);
            return dialog;
        }
        return null;
    }

    public void onReset() {
        this.chartData=new HashMap<String,Integer>();
        initChartData();
    }
    public void onPause() {
        senSensorManager.unregisterListener(this);
    }

    public void onResume() {
        senSensorManager.registerListener(this, senAccelerometer, SensorManager.SENSOR_DELAY_GAME);
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent) {
        if(networLoadet) {
            Sensor mySensor = sensorEvent.sensor;

            if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER) {
                x =  sensorEvent.values[0]/ 9.800000000000001;
                y =  sensorEvent.values[1]/ 9.800000000000001;
                z = sensorEvent.values[2]/ 9.800000000000001;
                if (oneTime) {
                    setCurrVector(x, y, z);
                    oneTime = false;
                }

                // Log.d("acc",""+x+" "+y+" "+z);
                data.add(new Double(x+y+z));
                counter++;
                curTime = System.currentTimeMillis();
                degree(x,y,z);
                if ((curTime - lastUpdate) > TIMEWINDOW) {
                    filter() ;
                    calcDiff();
                    testData(maxArrayVariance(),maxDegree, rangeMax, rangeMin, rangeMax - rangeMin, calcVariance(calcMean()));
                    lastUpdate = curTime;
                    /*
                    int i=data.size();
                    int j= (int)(counter/2);
                    for(int k=i; k>j ; k--){
                        data.remove(0);
                    }*/
                    rangeMax=0;
                    rangeMin=0;
                    sum=0;
                    counter=-1;
                    oneTime = true;
                    maxDegree=0;

                }
            }
        }
    }

    private void testData(double maxArrayVariance,double maxDegree , double rangeMax, double rangeMin, double range, double var) {
        if(Double.isNaN(maxArrayVariance) || Double.isNaN(maxDegree) || Double.isNaN(rangeMax) || Double.isNaN(rangeMin) || Double.isNaN(range) || Double.isNaN(var))
            return;
        input[0] = maxArrayVariance;
        input[1] = maxDegree;
        input[2] = rangeMax;
        input[3] = rangeMin;
        input[4] = range;
        input[5] = var;
        // Log.d("input",""+input[0]+" "+input[1]+" "+input[2]+" "+input[3]+" "+input[4]+" "+input[5]);
        nnet.setInput(input);
        nnet.calculate();
        output[0] = nnet.getOutput()[0];
        output[1] = nnet.getOutput()[1];
        output[2] = nnet.getOutput()[2];
        output[3] = nnet.getOutput()[3];
        output[4] = nnet.getOutput()[4];
        output[5] = nnet.getOutput()[5];
        //Log.d("output", "" + output[0] + " " + output[1] + " " + output[2] + " " + output[3] + " " + output[4] + " " + output[5]);
        evaluateOutput();

    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    private void calcDiff(){
        if(!data.isEmpty()) {
            double temp = data.get(0).doubleValue();
            for (int i = 1; i < data.size(); i++) {
                temp = data.get(i).doubleValue() - temp;
                data.set(i - 1, new Double(temp));
                sum += temp;
                temp = data.get(i).doubleValue();
            }
            data.remove(data.size() - 1);
        }
    }
    private void getMinMax(double a) {
        if (a < rangeMin)
            rangeMin = a;
        if (rangeMax < a)
            rangeMax = a;
    }

    private double calcMean() {
        return sum / counter;
    }

    private double calcVariance(double mean) {
        variance = 0;
        for (int i = 0; i < data.size(); i++) {
            variance += (mean - data.get(i).doubleValue()) * (mean - data.get(i).doubleValue());
        }
        return variance / counter;
    }

    private double maxArrayVariance(){
        ArrayList <Double>temp=new ArrayList<Double>();
        double mean=0;
        double var=0;
        for(int i=1;i<data.size()-1;i++){
            getMinMax(data.get(i).doubleValue());
            if(data.get(i-1).doubleValue()<data.get(i).doubleValue() && data.get(i).doubleValue()>data.get(i+1).doubleValue())
            {
                temp.add(data.get(i));
                mean+=data.get(i).doubleValue();
            }
        }
        mean=mean/temp.size();

        return mean;
    }

    private String evaluateOutput() {
        double highest = 0;
        int index = 99;
        for (int i = 0; i < OUTPUTLENGTH; i++) {

            if (output[i] > highest) {
                highest = output[i];
                index = i;
            }
        }
        return activities[remember(index)];


    }
    private int remember(int currentActivity){
        int temp=lastActivity;
        if(lastActivity==1 && currentActivity==3)
            currentActivity= lastActivity;
        else if(lastActivity==2 && currentActivity==3)
            currentActivity= lastActivity;
        else {
            lastActivity= currentActivity;
        }
        // sendActivityToServer(currentActivity);
       /* if(currentActivity!=temp)
            playSound(currentActivity);
       */
        fillChartData(currentActivity);
        //Log.d("currentActivity", "" + remember(currentActivity));
        return currentActivity;
    }
    private void playSound(int i){
        stopSounds();

        switch(i){
            case 0: soundPool.play(1,1,1,1,0,1);
                break;
            case 1: soundPool.play(2,1,1,1,0,1);
                break;
            case 2: soundPool.play(3,1,1,1,0,1);
                break;
            case 3: soundPool.play(4,1,1,1,0,1);
                break;
            case 4: soundPool.play(5,1,1,1,0,1);
                break;
            case 5: soundPool.play(6,1,1,1,0,1);
                break;
        }

    }
    private void stopSounds(){
        for(int i=0;i<soundPoolMap.size();i++){
            soundPool.stop(i);
        }
    }
    private void sendActivityToServer(int activity){

        //new NetworkTask().execute(URL+"activity="+activity+"&date="+(System.currentTimeMillis()));

    }

    private void degree(double x,double y,double z){

        magA= Math.sqrt(Math.pow(currentVector[0], 2)+Math.pow(currentVector[1], 2)+Math.pow(currentVector[2], 2));
        magB= Math.sqrt(x*x+y*y+z*z);
        ans =(currentVector[0]*x)+(currentVector[1]*y)+(currentVector[2]*z);

        curDegree= (Math.acos(ans/(magA*magB)) * (180/Math.PI))/360;
        // if(curDegree<minDegree)
        //     minDegree=curDegree;
        if(maxDegree<curDegree)
            maxDegree=curDegree;
    }
    private void setCurrVector(double x,double y,double z){
        currentVector[0]=x;
        currentVector[1]=y;
        currentVector[2]=z;
    }
    private void filter() {

        int size =data.size();
        if(size>2) {
            for (int i = 0; i < size - 2; i++) {
                // System.out.print("" + data.get(i));
                data.set(i, data.get(i) - (data.get(i) + 2 * data.get(i + 1) + data.get(i + 2)) / 4);
                // System.out.print(";" + data.get(i) + "\n");
            }
            data.remove(size - 1);
            data.remove(size - 2);
        }

    }
    private void initChartData(){
        for(int i=0;i<this.activities.length;i++){
            chartData.put(activities[i],new Integer(0));
        }
    }
    private void fillChartData(int i){
        chartData.put(activities[i], chartData.get(activities[i]) + 1);
    }

    public HashMap<String, Integer> getChartData() {
        return chartData;
    }
}
/*
    private class NetworkTask extends AsyncTask<String, Void, HttpResponse> {
        @Override
        protected HttpResponse doInBackground(String... params) {
            String link = params[0];
            HttpGet request = new HttpGet(link);
            AndroidHttpClient client = AndroidHttpClient.newInstance("Android");
            try {
                return client.execute(request);
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            } finally {
                client.close();
            }
        }



        }
    }
}*/

