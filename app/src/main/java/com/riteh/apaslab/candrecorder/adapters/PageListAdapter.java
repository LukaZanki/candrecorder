package com.riteh.apaslab.candrecorder.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.riteh.apaslab.candrecorder.AssignActivity;
import com.riteh.apaslab.candrecorder.R;
import com.riteh.apaslab.candrecorder.objectclasses.AssignIcons;

import java.util.List;


public class PageListAdapter extends ArrayAdapter<String> {

    private List<String> items;
    private int layoutResourceId;
    private Context context;
    private int groupIndex;

    public PageListAdapter(Context context, int layoutResourceId, List<String> items,int groupIndex) {
        super(context, layoutResourceId, items);
        this.layoutResourceId = layoutResourceId;
        this.context = context;
        this.items = items;
        this.groupIndex=groupIndex;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View rowView = inflater.inflate(R.layout.pager_list_item, parent, false);
        TextView textView = (TextView) rowView.findViewById(R.id.pager_list_item_name);
        ImageView imageView = (ImageView) rowView.findViewById(R.id.pager_list_item_icons);
        textView.setText(items.get(position));
        AssignIcons.setIcon(context, imageView, items.get(position));
        if(AssignActivity.checked_items.containsKey(this.groupIndex))
            if(AssignActivity.checked_items.get(groupIndex)==position && !rowView.isSelected()) {
                rowView.setSelected(true);
                rowView.setActivated(true);
            }

        return rowView;
    }


    @Override
    public long getItemId(int position) {
        //String item = getItem(position);
        return position;
    }

    @Override
    public boolean hasStableIds() {
        return true;
    }

}

