package com.riteh.apaslab.candrecorder.objectclasses;

import android.location.Location;
import android.location.LocationListener;
import android.os.Bundle;

import com.riteh.apaslab.candrecorder.PlayActivity;

import java.io.FileOutputStream;
import java.util.ArrayList;

public class GPSLocation implements LocationListener {

    public static double latitude;
    public static double longitude;
    public static double altitude;
    public static double speed;
    public static double accuracy;
    public static double bearing;

    @Override
    public void onLocationChanged(Location loc)
    {
        latitude=loc.getLatitude();
        longitude=loc.getLongitude();
        altitude = loc.getAltitude();
        speed = loc.getSpeed();
        accuracy = loc.getAccuracy();
        bearing = loc.getBearing();


    }

    @Override
    public void onProviderDisabled(String provider)
    {
        //print "Currently GPS is Disabled";
    }
    @Override
    public void onProviderEnabled(String provider)
    {
        //print "GPS got Enabled";
    }
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras)
    {
    }
}