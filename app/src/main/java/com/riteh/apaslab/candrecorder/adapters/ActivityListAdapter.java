package com.riteh.apaslab.candrecorder.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.TextView;

import com.riteh.apaslab.candrecorder.objectclasses.Activity;
import com.riteh.apaslab.candrecorder.R;

import java.util.List;


public class ActivityListAdapter extends ArrayAdapter<Activity> {

	private List<Activity> items;
	private int layoutResourceId;
	private Context context;

	public ActivityListAdapter(Context context, int layoutResourceId, List<Activity> items) {
		super(context, layoutResourceId, items);
		this.layoutResourceId = layoutResourceId;
		this.context = context;
		this.items = items;
	}
	
	@SuppressLint("ViewHolder")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View row = convertView;
		ActivityHolder holder = null;

		LayoutInflater inflater = ((android.app.Activity) context).getLayoutInflater();
		row = inflater.inflate(layoutResourceId, parent, false);

		holder = new ActivityHolder();
		holder.activity = items.get(position);
		
		holder.removeActivity = (ImageButton)row.findViewById(R.id.activity_delete);
		holder.removeActivity.setTag(holder.activity);
		
		holder.editActivity = (ImageButton)row.findViewById(R.id.activity_edit);
		holder.editActivity.setTag(holder.activity);
		
		holder.detailsActivity = (ImageButton)row.findViewById(R.id.activity_details);
		holder.detailsActivity.setTag(holder.activity);
		
		holder.playActivity = (ImageButton)row.findViewById(R.id.activity_play);
		holder.playActivity.setTag(holder.activity);

		holder.name = (TextView)row.findViewById(R.id.activity_name);

		row.setTag(holder);

		setupItem(holder);
		return row;
	}
	
	private void setupItem(ActivityHolder holder) {
		holder.name.setText(holder.activity.getName());
	}
	
	public static class ActivityHolder {
		Activity activity;
		TextView name;
		ImageButton editActivity;
		ImageButton detailsActivity;
		ImageButton playActivity;
		ImageButton removeActivity;
	}
}
