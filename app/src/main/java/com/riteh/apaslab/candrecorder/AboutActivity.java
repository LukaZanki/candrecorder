package com.riteh.apaslab.candrecorder;

import android.app.Activity;
import android.os.Bundle;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.widget.TextView;

public class AboutActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_about);
		
		TextView about1 = (TextView) findViewById(R.id.about1);
		about1.append(Html.fromHtml(
				"<a href=\"" + getString(R.string.apaslab_homepage) + "\">" + getString(R.string.provided_by) + "</a> "));
		about1.setMovementMethod(LinkMovementMethod.getInstance());
		about1.append(getString(R.string.about_text2));
		about1.append(Html.fromHtml(
				"<a href=\"" + getString(R.string.server_homepage) + "\">" + getString(R.string.website_name) + "</a> "));
		about1.setMovementMethod(LinkMovementMethod.getInstance());
		
		TextView copyright = (TextView) findViewById(R.id.tv_copyright);
		copyright.append(getString(R.string.copyright2));
	}
}
