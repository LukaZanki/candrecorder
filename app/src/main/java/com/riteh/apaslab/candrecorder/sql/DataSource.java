package com.riteh.apaslab.candrecorder.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.riteh.apaslab.candrecorder.objectclasses.Activity;
import com.riteh.apaslab.candrecorder.objectclasses.FileDetails;
import com.riteh.apaslab.candrecorder.objectclasses.GroupList;

import java.util.ArrayList;
import java.util.List;


public class DataSource {
	// Database fields
	private static SQLiteDatabase database;
	private static SQLiteHelper dbHelper;

	private String[] allColumns_group_list = new String[]{
			SQLiteHelper.COLUMN_ID_GROUP_LIST,
			SQLiteHelper.COLUMN_NAME_GL,
			SQLiteHelper.COLUMN_TABLE_NAME };

	private String[] allColumns_style = new String[]{
			SQLiteHelper.COLUMN_ID_STYLE,
			SQLiteHelper.COLUMN_TYPE_STYLE };

	private String[] allColumns_surface_type = new String[]{
			SQLiteHelper.COLUMN_ID_SURFACE,
			SQLiteHelper.COLUMN_TYPE_SURF };

	private String[] allColumns_sensor_placement = new String[]{
			SQLiteHelper.COLUMN_ID_SENSOR,
			SQLiteHelper.COLUMN_TYPE_SEN };

	private String[] allColumns_activity_type = new String[]{
			SQLiteHelper.COLUMN_ID_ACT_TYPE,
			SQLiteHelper.COLUMN_TYPE_ACT };

	private String[] allColumns_deleted_act  = new String[]{
			SQLiteHelper.COLUMN_ID_DELETED_ACT,
			SQLiteHelper.COLUMN_ID_ACT};

	private String[] allColumns_activity = new String[]{
			SQLiteHelper.COLUMN_ID_ACT,
			SQLiteHelper.COLUMN_NAME_ACT,
			SQLiteHelper.COLUMN_TIMESTAMP,
			SQLiteHelper.COLUMN_TOTAL_TIME,
			SQLiteHelper.COLUMN_ID_ACT_TYPE,
			SQLiteHelper.COLUMN_ID_SENSOR,
			SQLiteHelper.COLUMN_ID_SURFACE,
			SQLiteHelper.COLUMN_ID_STYLE };

	private String[] allColumns_recording = new String[]{
			SQLiteHelper.COLUMN_ID_REC,
			SQLiteHelper.COLUMN_NAME_REC,
			SQLiteHelper.COLUMN_TIMESTAMP,
			SQLiteHelper.COLUMN_DURATION,
			SQLiteHelper.COLUMN_SESSION_ID,
			SQLiteHelper.COLUMN_IS_UPLOADED,
			SQLiteHelper.COLUMN_ACCELEROMETER,
			SQLiteHelper.COLUMN_AMBIENT_TEMP,
			SQLiteHelper.COLUMN_GRAVITY,
			SQLiteHelper.COLUMN_GYROSCOPE,
			SQLiteHelper.COLUMN_LIGHT,
			SQLiteHelper.COLUMN_LINEAR_ACC,
			SQLiteHelper.COLUMN_MAGNETIC_FIELD,
			SQLiteHelper.COLUMN_PRESSURE,
			SQLiteHelper.COLUMN_PROXIMITY,
			SQLiteHelper.COLUMN_RELATIVE_HUMIDITY,
			SQLiteHelper.COLUMN_GPS,
			SQLiteHelper.COLUMN_ID_ACT};

	public DataSource(Context context) {
		dbHelper = new SQLiteHelper(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}


	// CREATE RECORD
	public FileDetails createRecord(String name, long timestamp, long duration, int session_id,
			int is_uploaded, String acceleration, String ambientTemp,
			String gravity, String gyroscope, String light, String linearAcc, String magneticField,
			String pressure, String proximity, String relativeHumidity, String gps, long id_act) {
		ContentValues values = new ContentValues();
		values.put(SQLiteHelper.COLUMN_NAME_REC, name);
		values.put(SQLiteHelper.COLUMN_TIMESTAMP, timestamp);
		values.put(SQLiteHelper.COLUMN_DURATION, duration);
		values.put(SQLiteHelper.COLUMN_SESSION_ID, session_id);
		values.put(SQLiteHelper.COLUMN_IS_UPLOADED, is_uploaded);
		values.put(SQLiteHelper.COLUMN_ACCELEROMETER, acceleration);
		values.put(SQLiteHelper.COLUMN_AMBIENT_TEMP, ambientTemp);
		values.put(SQLiteHelper.COLUMN_GRAVITY, gravity);
		values.put(SQLiteHelper.COLUMN_GYROSCOPE, gyroscope);
		values.put(SQLiteHelper.COLUMN_LIGHT, light);
		values.put(SQLiteHelper.COLUMN_LINEAR_ACC, linearAcc);
		values.put(SQLiteHelper.COLUMN_MAGNETIC_FIELD, magneticField);
		values.put(SQLiteHelper.COLUMN_PRESSURE, pressure);
		values.put(SQLiteHelper.COLUMN_PROXIMITY, proximity);
		values.put(SQLiteHelper.COLUMN_RELATIVE_HUMIDITY, relativeHumidity);
		values.put(SQLiteHelper.COLUMN_GPS, gps);
		values.put(SQLiteHelper.COLUMN_ID_ACT, id_act);

		long insertId = database.insert(SQLiteHelper.TABLE_NAME_RECORDING, null,
				values);
		Cursor cursor = database.query(SQLiteHelper.TABLE_NAME_RECORDING,
				allColumns_recording, SQLiteHelper.COLUMN_ID_REC + " = " + insertId, null,
				null, null, null);
		cursor.moveToFirst();
		FileDetails newRecord = cursorToFileDesc(cursor);
		cursor.close();
		return newRecord;
	}

	// CREATE ACTIVITY
	public void createActivity(String name, long timestamp, long total_time, long id_act_type, long id_sensor, long id_surface_type, long id_style) {

		ContentValues values = new ContentValues();
		values.put(SQLiteHelper.COLUMN_NAME_ACT, name);
		values.put(SQLiteHelper.COLUMN_TIMESTAMP, timestamp);
		values.put(SQLiteHelper.COLUMN_TOTAL_TIME, total_time);
		values.put(SQLiteHelper.COLUMN_ID_ACT_TYPE, id_act_type);
		values.put(SQLiteHelper.COLUMN_ID_SENSOR, id_sensor);
		values.put(SQLiteHelper.COLUMN_ID_SURFACE, id_surface_type);
		values.put(SQLiteHelper.COLUMN_ID_STYLE, id_style);

		long insertId = database.insert(SQLiteHelper.TABLE_NAME_ACTIVITY, null,
				values);
		/*
		open();
		Cursor cursor = database.query(SQLiteHelper.TABLE_NAME_ACTIVITY,
				allColumns_activity, SQLiteHelper.COLUMN_ID_ACT + " = " + insertId, null,
				null, null, null);
		cursor.moveToFirst();
		Activity newActivity = cursorToActivity(cursor);
		cursor.close();
		return newActivity;
		*/
	}

	public void setTotalTimeActivity(long id, long timeOnActivity){
		long time = getTotalTimeOnActivity(id);
		timeOnActivity += time;
		ContentValues values = new ContentValues();
		values.put(SQLiteHelper.COLUMN_TOTAL_TIME, timeOnActivity);
		database.update(SQLiteHelper.TABLE_NAME_ACTIVITY, values, "" + SQLiteHelper.COLUMN_ID_ACT + "=" + id + ";", null);
	}

	public long getTotalTimeOnActivity(long id){
		long time = 0;
		Cursor cursor = database.query(SQLiteHelper.TABLE_NAME_ACTIVITY,
				allColumns_activity, SQLiteHelper.COLUMN_ID_ACT + " = " + id, null, null, null, null);

		if (cursor.moveToFirst()) {
			cursor.moveToFirst();
			time = cursor.getLong(3);
			cursor.close();
		}
		return time;

	}

	public String getActivityType (int id){
		String type = "";
		Cursor cursor = database.query(SQLiteHelper.TABLE_NAME_ACTIVITY_TYPE,
				allColumns_activity_type, SQLiteHelper.COLUMN_ID_ACT_TYPE + " = " + id, null, null, null, null);

		if (cursor.moveToFirst()) {
			cursor.moveToFirst();
			type = cursor.getString(1);
			cursor.close();
		}
		return type;

	}

	public String getSensorPlacement (int id){
		String type = "";
		Cursor cursor = database.query(SQLiteHelper.TABLE_NAME_SENSOR_PLACEMENT,
				allColumns_sensor_placement, SQLiteHelper.COLUMN_ID_SENSOR + " = " + id, null, null, null, null);

		if (cursor.moveToFirst()) {
			cursor.moveToFirst();
			type = cursor.getString(1);
			cursor.close();
		}
		return type;

	}

	public String getSurfaceType (int id){
		String type = "";
		Cursor cursor = database.query(SQLiteHelper.TABLE_NAME_SURFACE_TYPE,
				allColumns_surface_type, SQLiteHelper.COLUMN_ID_SURFACE + " = " + id, null, null, null, null);

		if (cursor.moveToFirst()) {
			cursor.moveToFirst();
			type = cursor.getString(1);
			cursor.close();
		}
		return type;

	}

	public String getStyle (int id){
		String type = "";
		Cursor cursor = database.query(SQLiteHelper.TABLE_NAME_STYLE,
				allColumns_style, SQLiteHelper.COLUMN_ID_STYLE + " = " + id, null, null, null, null);

		if (cursor.moveToFirst()) {
			cursor.moveToFirst();
			type = cursor.getString(1);
			cursor.close();
		}
		return type;

	}

	public long getActivityTypeID(String name){
		long id=0;
		Cursor cursor = database.query(SQLiteHelper.TABLE_NAME_ACTIVITY_TYPE,
				allColumns_activity_type, SQLiteHelper.COLUMN_TYPE_ACT + " = " + name, null, null, null, null);
		if (cursor.moveToFirst()) {
			cursor.moveToFirst();
			id = cursor.getLong(0);
			cursor.close();
		}
		return id;
	}

	public long getSensorPlacementID(String name){
		long id=0;
		Cursor cursor = database.query(SQLiteHelper.TABLE_NAME_SENSOR_PLACEMENT,
				allColumns_sensor_placement, SQLiteHelper.COLUMN_TYPE_SEN + " = " + name, null, null, null, null);
		if (cursor.moveToFirst()) {
			cursor.moveToFirst();
			id = cursor.getLong(0);
			cursor.close();
		}
		return id;
	}

	public long getSurfaceTypeID(String name){
		long id=0;
		Cursor cursor = database.query(SQLiteHelper.TABLE_NAME_SURFACE_TYPE,
				allColumns_surface_type, SQLiteHelper.COLUMN_TYPE_SURF + " = " + name, null, null, null, null);
		if (cursor.moveToFirst()) {
			cursor.moveToFirst();
			id = cursor.getLong(0);
			cursor.close();
		}
		return id;
	}

	public long getStyleTypeID(String name){
		long id=0;
		Cursor cursor = database.query(SQLiteHelper.TABLE_NAME_STYLE,
				allColumns_style, SQLiteHelper.COLUMN_TYPE_STYLE + " = " + name, null, null, null, null);
		if (cursor.moveToFirst()) {
			cursor.moveToFirst();
			id = cursor.getLong(0);
			cursor.close();
		}
		return id;
	}

	public List<GroupList> getAllGroupList() {
		List<GroupList> listGroupList = new ArrayList<GroupList>();
		Cursor cursor = database.query(SQLiteHelper.TABLE_NAME_GROUP_LIST,
				allColumns_group_list, null, null, null, null, null);

		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			GroupList groupList = cursorToGroupList(cursor);
			listGroupList.add(groupList);
			cursor.moveToNext();
		}

		cursor.close();
		return listGroupList;
	}

	private GroupList cursorToGroupList(Cursor cursor) {
		GroupList groupList = new GroupList();
		groupList.setId_group_list(cursor.getLong(0));
		groupList.setName_group_list(cursor.getString(1));
		groupList.setTable_name_group_list(cursor.getString(2));
		return groupList;
	}

	public Activity getActivityWithTypeName(long id) {
		String joinQuery = "SELECT act." + SQLiteHelper.COLUMN_ID_ACT + ", act." + SQLiteHelper.COLUMN_NAME_ACT + ", act." + SQLiteHelper.COLUMN_TIMESTAMP + ", act." + SQLiteHelper.COLUMN_TOTAL_TIME + ", act."
				+ SQLiteHelper.COLUMN_ID_ACT_TYPE + ", act." + SQLiteHelper.COLUMN_ID_SENSOR + ", act." + SQLiteHelper.COLUMN_ID_SURFACE + ", act." + SQLiteHelper.COLUMN_ID_STYLE + ", acttype."
				+ SQLiteHelper.COLUMN_TYPE_ACT + ", sensor." + SQLiteHelper.COLUMN_TYPE_SEN + ", surf." + SQLiteHelper.COLUMN_TYPE_SURF + ", style." + SQLiteHelper.COLUMN_TYPE_STYLE
				+ " FROM " + SQLiteHelper.TABLE_NAME_ACTIVITY + " as act, " + SQLiteHelper.TABLE_NAME_ACTIVITY_TYPE + " as acttype, " + SQLiteHelper.TABLE_NAME_SENSOR_PLACEMENT +
				" as sensor, " + SQLiteHelper.TABLE_NAME_SURFACE_TYPE + " as surf, " + SQLiteHelper.TABLE_NAME_STYLE +
				" as style WHERE act." + SQLiteHelper.COLUMN_ID_ACT + " = "+ id
				+ " AND act." + SQLiteHelper.COLUMN_ID_ACT_TYPE + " = acttype." + SQLiteHelper.COLUMN_ID_ACT_TYPE
				+" AND act." + SQLiteHelper.COLUMN_ID_SENSOR + " = sensor." + SQLiteHelper.COLUMN_ID_SENSOR
				+ " AND act." + SQLiteHelper.COLUMN_ID_SURFACE + " = surf." + SQLiteHelper.COLUMN_ID_SURFACE
				+ " AND act." + SQLiteHelper.COLUMN_ID_STYLE + " = style." + SQLiteHelper.COLUMN_ID_STYLE+ " GROUP BY " + SQLiteHelper.COLUMN_ID_ACT + ";";
		Cursor cursor = database.rawQuery(joinQuery, null);
		Activity itemToFetch = new Activity();
		if (cursor.moveToFirst()) {
			cursor.moveToFirst();
			itemToFetch = cursorToActivity(cursor);
		}
		cursor.close();
		return itemToFetch;
	}

	public List<Activity> getAllActivityWithTypeName() {
		List<Activity> activities = new ArrayList<Activity>();
		long [] id = getAllDeletedActivities();
		boolean tmpHelp = false;
		String joinQuery = "SELECT act." + SQLiteHelper.COLUMN_ID_ACT + ", act." + SQLiteHelper.COLUMN_NAME_ACT + ", act." + SQLiteHelper.COLUMN_TIMESTAMP + ", act." + SQLiteHelper.COLUMN_TOTAL_TIME + ", act."
				+ SQLiteHelper.COLUMN_ID_ACT_TYPE + ", act." + SQLiteHelper.COLUMN_ID_SENSOR + ", act." + SQLiteHelper.COLUMN_ID_SURFACE + ", act." + SQLiteHelper.COLUMN_ID_STYLE + ",acttype."
				+ SQLiteHelper.COLUMN_TYPE_ACT + ", sensor." + SQLiteHelper.COLUMN_TYPE_SEN + ", surf." + SQLiteHelper.COLUMN_TYPE_SURF + ", style." + SQLiteHelper.COLUMN_TYPE_STYLE
				+ " FROM " + SQLiteHelper.TABLE_NAME_ACTIVITY + " as act, " + SQLiteHelper.TABLE_NAME_ACTIVITY_TYPE + " as acttype, " + SQLiteHelper.TABLE_NAME_SENSOR_PLACEMENT +
				" as sensor, " + SQLiteHelper.TABLE_NAME_SURFACE_TYPE + " as surf, " + SQLiteHelper.TABLE_NAME_STYLE +
				" as style WHERE act." + SQLiteHelper.COLUMN_ID_ACT_TYPE + " = acttype." + SQLiteHelper.COLUMN_ID_ACT_TYPE
				+" AND act." + SQLiteHelper.COLUMN_ID_SENSOR + " = sensor." + SQLiteHelper.COLUMN_ID_SENSOR
				+ " AND act." + SQLiteHelper.COLUMN_ID_SURFACE + " = surf." + SQLiteHelper.COLUMN_ID_SURFACE
				+ " AND act." + SQLiteHelper.COLUMN_ID_STYLE + " = style." + SQLiteHelper.COLUMN_ID_STYLE+ " GROUP BY " + SQLiteHelper.COLUMN_ID_ACT + ";";
		try {
			Cursor cursor = database.rawQuery(joinQuery, null);
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				Activity activity = cursorToActivity(cursor);
				for (int i=0;i<id.length; i++) {
					if (activity.getId() == id[i]) {
						tmpHelp = true;
						break;
					}
				}
				if (!tmpHelp) {
					activities.add(activity);
				}
				tmpHelp = false;
				cursor.moveToNext();
			}
			cursor.close();
		}catch (SQLException e){
			e.printStackTrace();
		}
		return activities;
	}

	public long[] getAllDeletedActivities(){
		long[] id;
		Cursor cursor = database.query(SQLiteHelper.TABLE_NAME_DELETED_ACTIVITIES,
				allColumns_deleted_act, null, null, null, null, null);

		cursor.moveToFirst();
		id = new long[cursor.getCount()];
		for (int i=0;!cursor.isAfterLast();i++) {
			id[i] = cursor.getLong(1);
			cursor.moveToNext();
		}
		cursor.close();
		return id;
	}

	public String[] getAllActivityType() {
		String [] typeNameAll = null;
		Cursor cursor = database.query(SQLiteHelper.TABLE_NAME_ACTIVITY_TYPE,
				allColumns_activity_type, null, null, null, null, null);

		cursor.moveToFirst();
		typeNameAll = new String[cursor.getCount()];
		for (int i=0;!cursor.isAfterLast();i++) {
			typeNameAll[i] = cursor.getString(1);;
			cursor.moveToNext();
		}
		cursor.close();
		return typeNameAll;
	}

	public String[] getAllSensorPlacement() {
		String [] typeNameAll = null;
		Cursor cursor = database.query(SQLiteHelper.TABLE_NAME_SENSOR_PLACEMENT,
				allColumns_sensor_placement, null, null, null, null, null);

		cursor.moveToFirst();
		typeNameAll = new String[cursor.getCount()];
		for (int i=0;!cursor.isAfterLast();i++) {
			typeNameAll[i] = cursor.getString(1);;
			cursor.moveToNext();
		}
		cursor.close();
		return typeNameAll;
	}

	public String[] getAllSurfaceType() {
		String [] typeNameAll = null;
		Cursor cursor = database.query(SQLiteHelper.TABLE_NAME_SURFACE_TYPE,
				allColumns_surface_type, null, null, null, null, null);

		cursor.moveToFirst();
		typeNameAll = new String[cursor.getCount()];
		for (int i=0;!cursor.isAfterLast();i++) {
			typeNameAll[i] = cursor.getString(1);;
			cursor.moveToNext();
		}
		cursor.close();
		return typeNameAll;
	}

	public String[] getAllStyleType() {
		String [] typeNameAll = null;
		Cursor cursor = database.query(SQLiteHelper.TABLE_NAME_STYLE,
				allColumns_style, null, null, null, null, null);

		cursor.moveToFirst();
		typeNameAll = new String[cursor.getCount()];
		for (int i=0;!cursor.isAfterLast();i++) {
			typeNameAll[i] = cursor.getString(1);;
			cursor.moveToNext();
		}
		cursor.close();
		database.close();
		return typeNameAll;
	}

	public void editActivity (long id, String name, long timestamp, long total_time, long id_act_type, long id_sensor, long id_surface_type, long id_style){
		Activity act = getActivityWithTypeName(id);
		ContentValues values = new ContentValues();
		values.put(SQLiteHelper.COLUMN_NAME_ACT, name);
		values.put(SQLiteHelper.COLUMN_TIMESTAMP, timestamp);
		values.put(SQLiteHelper.COLUMN_TOTAL_TIME, total_time);
		values.put(SQLiteHelper.COLUMN_ID_ACT_TYPE, id_act_type);
		values.put(SQLiteHelper.COLUMN_ID_SENSOR, id_sensor);
		values.put(SQLiteHelper.COLUMN_ID_SURFACE, id_surface_type);
		values.put(SQLiteHelper.COLUMN_ID_STYLE, id_style);
		// EDIT AKTIVNOSTI
		database.update(SQLiteHelper.TABLE_NAME_ACTIVITY, values, SQLiteHelper.COLUMN_ID_ACT + "=" + id, null);
		if (!getAllRecordingsWithActivityID(id).isEmpty()) {
			// UNOS STARE AKTIVNOSTI KAO NOVI RED U TABLICI
			ContentValues deletedValues = new ContentValues();
			deletedValues.put(SQLiteHelper.COLUMN_NAME_ACT, act.getName());
			deletedValues.put(SQLiteHelper.COLUMN_TIMESTAMP, act.getTimestamp());
			deletedValues.put(SQLiteHelper.COLUMN_TOTAL_TIME, act.getTotal_time());
			deletedValues.put(SQLiteHelper.COLUMN_ID_ACT_TYPE, act.getId_act_type());
			deletedValues.put(SQLiteHelper.COLUMN_ID_SENSOR, act.getId_sensor());
			deletedValues.put(SQLiteHelper.COLUMN_ID_SURFACE, act.getId_surface());
			deletedValues.put(SQLiteHelper.COLUMN_ID_STYLE, act.getId_style());
			long insertID = database.insert(SQLiteHelper.TABLE_NAME_ACTIVITY, null, deletedValues);
			// MIJENJANJE ID-a SVIM RECORDINGSIMA IZBRISANE AKTIVNOSTI
			editRecording(id, insertID);
			// OZNAČAVANJE STARE AKTIVNOSTI KAO IZBRISANE
			ContentValues valuesDel = new ContentValues();
			valuesDel.put(SQLiteHelper.COLUMN_ID_ACT, insertID);
			database.insert(SQLiteHelper.TABLE_NAME_DELETED_ACTIVITIES, null, valuesDel);
		}
	}

	public void editRecording(long id, long newId){

		ContentValues values = new ContentValues();
		values.put(SQLiteHelper.COLUMN_ID_ACT, newId);

		database.update(SQLiteHelper.TABLE_NAME_RECORDING, values, SQLiteHelper.COLUMN_ID_ACT + "=" + id, null);

	}

	static public void upload_file(long id){
		database = dbHelper.getWritableDatabase();
		ContentValues data=new ContentValues();
		data.put(SQLiteHelper.COLUMN_IS_UPLOADED, 1);
		int rows = database.update(SQLiteHelper.TABLE_NAME_RECORDING, data,
				SQLiteHelper.COLUMN_ID_REC + "='" + id + "'", null);
		Log.d("ROWS UPDATED", String.valueOf(rows));
		database.close();
	}

	public void deleteActivity(Activity itemToRemove) {
		List<FileDetails> recordings = new ArrayList<FileDetails>();
		recordings = getAllRecordingsWithActivityID(itemToRemove.getId());
		if (!recordings.isEmpty()) {
			ContentValues values = new ContentValues();
			values.put(SQLiteHelper.COLUMN_ID_ACT, itemToRemove.getId());

			long insertId = database.insert(SQLiteHelper.TABLE_NAME_DELETED_ACTIVITIES, null,
					values);

		}else{
			database.delete(SQLiteHelper.TABLE_NAME_ACTIVITY, SQLiteHelper.COLUMN_ID_ACT
					+ " = " + itemToRemove.getId(), null);
		}
	}

	public List<FileDetails> getAllRecordingsWithActivityID(long id) {
		List<FileDetails> recordings = new ArrayList<FileDetails>();

		Cursor cursor = database.query(SQLiteHelper.TABLE_NAME_RECORDING,
				allColumns_recording, SQLiteHelper.COLUMN_ID_ACT + " = " + id, null, null, null, null);
		if (cursor.moveToFirst()) {
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				FileDetails recording = cursorToFileDesc(cursor);
				recording.setActivity(getActivityWithTypeName(recording.getID_Activity()));
				recordings.add(recording);
				cursor.moveToNext();
			}
			cursor.close();
		}
		return recordings;
	}

	public void deleteRecording(long id_rec, long id_act) {
		database.delete(SQLiteHelper.TABLE_NAME_RECORDING, SQLiteHelper.COLUMN_ID_REC
				+ " = " + id_rec, null);
		List<FileDetails> recordings = new ArrayList<FileDetails>();
		recordings = getAllRecordingsWithActivityID(id_act);
		boolean isDeleted = isActivityDeleted(id_act);
		if (recordings.isEmpty() && isDeleted){
			database.delete(SQLiteHelper.TABLE_NAME_ACTIVITY, SQLiteHelper.COLUMN_ID_ACT
					+ " = " + id_act, null);
			database.delete(SQLiteHelper.TABLE_NAME_DELETED_ACTIVITIES, SQLiteHelper.COLUMN_ID_ACT
					+ " = " + id_act, null);
		}
	}

	public boolean isActivityDeleted(long id){
		boolean temp = false;
		long [] deletedID = getAllDeletedActivities();
		for (int i=0;i<deletedID.length; i++) {
			if (id == deletedID[i]) {
				temp = true;
				break;
			}
		}
		return temp;
	}

	public void deleteRecordings() {
		database.delete(SQLiteHelper.TABLE_NAME_RECORDING, null, null);
		//CLEANUP
		deleteDeletedActivities();
	}

	public void deleteDeletedActivities(){
		long [] deletedID = getAllDeletedActivities();
		for (int i = 0; i < deletedID.length; i++) {
			database.delete(SQLiteHelper.TABLE_NAME_ACTIVITY, SQLiteHelper.COLUMN_ID_ACT
					+ " = " + deletedID[i], null);
		}
		database.delete(SQLiteHelper.TABLE_NAME_DELETED_ACTIVITIES, null, null);
	}

	public List<FileDetails> getAllRecordings() {
		List<FileDetails> recordings = new ArrayList<FileDetails>();

		Cursor cursor = database.query(SQLiteHelper.TABLE_NAME_RECORDING,
				allColumns_recording, null, null, null, null, null);
		if (cursor.moveToFirst()) {
			cursor.moveToFirst();
			while (!cursor.isAfterLast()) {
				FileDetails recording = cursorToFileDesc(cursor);
				recording.setActivity(getActivityWithTypeName(recording.getID_Activity()));
				recordings.add(recording);
				cursor.moveToNext();
			}
		}
		cursor.close();
		return recordings;
	}

	public boolean sessionExists(String SID){
		Cursor cursor = database.query(SQLiteHelper.TABLE_NAME_RECORDING,
				allColumns_recording, SQLiteHelper.COLUMN_SESSION_ID + " = " + Integer.parseInt(SID),
				null, null, null, null);
		if(cursor.getCount() == 0) return false;
		else return true;
	}

	private Activity cursorToActivity(Cursor cursor) {
		Activity activity = new Activity();
		activity.setId(cursor.getLong(0));
		activity.setName(cursor.getString(1));
		activity.setTimestamp(cursor.getLong(2));
		activity.setTotal_time(cursor.getLong(3));
		activity.setId_act_type(cursor.getLong(4));
		activity.setId_sensor(cursor.getLong(5));
		activity.setId_surface(cursor.getLong(6));
		activity.setId_style(cursor.getLong(7));
		activity.setAct_type(cursor.getString(8));
		activity.setSensor_place(cursor.getString(9));
		activity.setSurface_type(cursor.getString(10));
		activity.setStyle(cursor.getString(11));
		return activity;
	}

	private FileDetails cursorToFileDesc(Cursor cursor) {
		FileDetails record = new FileDetails();
		record.setId(cursor.getLong(0));
		record.setName(cursor.getString(1));
		record.setTimestamp(cursor.getLong(2));
		record.setDuration(cursor.getLong(3));
		record.setSession_id(cursor.getInt(4));
		record.setIs_uploaded(cursor.getInt(5));
		record.setAccelerometer(cursor.getString(6));
		record.setAmbientTemp(cursor.getString(7));
		record.setGravity(cursor.getString(8));
		record.setGyroscope(cursor.getString(9));
		record.setLight(cursor.getString(10));
		record.setLinearAccel(cursor.getString(11));
		record.setMagneticField(cursor.getString(12));
		record.setPressure(cursor.getString(13));
		record.setProximity(cursor.getString(14));
		record.setRelativeHumidity(cursor.getString(15));
		record.setGPS(cursor.getString(16));
		record.setID_Activity(cursor.getLong(17));
		return record;
	}

}
