package com.riteh.apaslab.candrecorder.clientside;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.riteh.apaslab.candrecorder.sql.DataSource;
import com.riteh.apaslab.candrecorder.R;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.HttpResponse;
import org.apache.http.HttpVersion;
import org.apache.http.auth.AuthScope;
import org.apache.http.auth.UsernamePasswordCredentials;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ByteArrayEntity;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.entity.mime.content.ContentBody;
import org.apache.http.entity.mime.content.FileBody;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.CoreProtocolPNames;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class HttpPostOperations extends Activity {
	EditText un, pw;
	TextView error, not_registered;
	CheckBox cb;
	Button ok;

	private String resp;
	private String username = "";
	private String password = "";
	private StringBuilder errorMsg = new StringBuilder();
	private ProgressDialog barProgressDialog;
	private ProgressDialog ringProgressDialog;
	private String key;

	int session_id;
	long _id;
	SharedPreferences shared_pref;

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_login);
		
		barProgressDialog = new ProgressDialog(HttpPostOperations.this);

		//data collection details
		session_id = getIntent().getIntExtra("session_id", 0);
		_id = getIntent().getLongExtra("_id", 0);

		shared_pref = getSharedPreferences(getString(R.string.app_name), MODE_PRIVATE);

		//GUI components
		un = (EditText) findViewById(R.id.et_un);
		pw = (EditText) findViewById(R.id.et_pw);
		ok = (Button) findViewById(R.id.btn_login);
		error = (TextView) findViewById(R.id.tv_error);
		cb = (CheckBox) findViewById(R.id.checkBox1);
		not_registered = (TextView) findViewById(R.id.tv_not_registered);
		not_registered.append(Html.fromHtml(
                "<a href=\"" + getString(R.string.server_link) + "\">" + getString(R.string.website_name) + "</a> "));
		not_registered.setMovementMethod(LinkMovementMethod.getInstance());

		cb.setChecked(readRememberMe());
		if( cb.isChecked() ){
			un.setText(username);
			pw.setText(password);
			cb.setChecked(readRememberMe());
		}

		if( isNetworkConnected() ){
			error.setTextColor(Color.rgb(0, 126, 0));
			error.setText(getString(R.string.internet_avilable));
		} else {
			error.setTextColor(Color.RED);
			error.setText(getString(R.string.internet_unavilable));
		}

		ok.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				//startFileUpload();
				//check if input in TextFields are OK
				if( isInputCorrect() ){
					ringProgressDialog = ProgressDialog.show(
                            HttpPostOperations.this, getString(R.string.ring_progress_title),
                            getString(R.string.ring_progress_text), true);
					ringProgressDialog.setCancelable(true);
					new LoginOperation().execute("");
				} else {
					error.setTextColor(Color.RED);
					error.setText(errorMsg.toString());
					errorMsg.delete(0, errorMsg.length());
				}
			}
		});
	}

	private class LoginOperation extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {

			// Build the JSON object to pass parameters
			JSONObject jsonObj = new JSONObject();
			try {
				jsonObj.put("username", un.getText().toString());
				jsonObj.put("password", pw.getText().toString());
			} catch (JSONException e) {
				e.printStackTrace();
			}

			HttpHost targetHost = new HttpHost("cand.riteh.hr", 80, "http");
			HttpPost httpPost = new HttpPost("/api/app/user/login/");

			try {
				httpPost.setEntity(new ByteArrayEntity(jsonObj.toString().getBytes("UTF8")));
			} catch (UnsupportedEncodingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			DefaultHttpClient client = new DefaultHttpClient();

			client.getCredentialsProvider().setCredentials(
					new AuthScope(targetHost.getHostName(), targetHost.getPort()),
					new UsernamePasswordCredentials("username", "password"));

			try {

				

				HttpResponse response = client.execute(targetHost, httpPost);
				HttpEntity entity = response.getEntity();
				resp = EntityUtils.toString(entity);
				Log.d("response", response.toString());
				jsonObj = new JSONObject(resp);
				Log.d("HTTP", "HTTP: OK");
				
				
				if(jsonObj.getString("success").equals("true")){
					Log.d("key:", jsonObj.getString("key"));

					if(jsonObj.getString("key").length() == 10){
						key = jsonObj.getString("key");
						writeRememberMe(un.getText().toString(),pw.getText().toString(),cb.isChecked(), key);
					} else {
						errorMsg.append(getString(R.string.server_wrong_key_size));
					}
				} else {
					Log.d("failure:", jsonObj.getString("reason"));
					errorMsg.append(jsonObj.getString("reason"));
				}

			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				Log.e("HTTP", "Error in http connection " + e.toString());
				errorMsg.append(getString(R.string.server_not_reachable));
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				Log.e("HTTP", "Error in http connection " + e.toString());
				errorMsg.append(getString(R.string.server_not_reachable));
				e.printStackTrace();
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				Log.e("JSON parsing", "Error in parsing JSON object: " + e.toString());
				errorMsg.append(getString(R.string.server_not_reachable));
				e.printStackTrace();
			}
			ringProgressDialog.dismiss();
			return "Executed";
		}

		@Override
		protected void onPostExecute(String result) {
			try {
				if(errorMsg.length() == 0){
					error.setTextColor(Color.rgb(0, 126, 0));
					error.setText(getString(R.string.login_success));

					AlertDialog.Builder alert = new AlertDialog.Builder(HttpPostOperations.this);

					alert.setTitle(getString(R.string.dialog_confirm_title));
					alert.setIcon(R.drawable.ic_info);

					alert.setMessage(getString(R.string.dialog_confirm_text));

					alert.setPositiveButton(getString(R.string.dialog_confirm_positive_btn), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
							startFileUpload();
						}
					});

					alert.setNegativeButton(getString(R.string.dialog_no), new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int whichButton) {
							finish();
						}
					});
					alert.show();
				} else throw new Exception(errorMsg.toString());

			} catch (Exception e) {
				error.setTextColor(Color.RED);
				error.setText(e.getMessage());
			} finally {
				errorMsg.delete(0, errorMsg.length());
			}
		}

		@Override
		protected void onPreExecute() {}

		@Override
		protected void onProgressUpdate(Void... values) {}
	}


	private class FileUploadOperation extends AsyncTask<String, Void, String> {

		@Override
		protected String doInBackground(String... params) {

			HttpClient httpclient = new DefaultHttpClient();
			HttpHost targetHost = new HttpHost("cand.riteh.hr", 80, "http");
			HttpPost httpPost = new HttpPost("/user/file/upload/app/");
			httpclient.getParams().setParameter(CoreProtocolPNames.PROTOCOL_VERSION, HttpVersion.HTTP_1_1);


			String boundary = "-------------" + System.currentTimeMillis();
			httpPost.setHeader("Content-type", "multipart/form-data; boundary="+boundary);

			final File myFile = new File(Environment.getExternalStorageDirectory()+"/"+
					getString(R.string.app_name)+"/"+
					params[0].substring(params[0].length()-8, params[0].length()-4)+"/"
					, params[0]);

			MultipartEntityBuilder builder = MultipartEntityBuilder.create();
			builder.setMode(HttpMultipartMode.BROWSER_COMPATIBLE);
			builder.setBoundary(boundary);

			System.out.println(myFile.getAbsolutePath());
			ContentBody cbFile = new FileBody(myFile);
			builder.addTextBody("key", key);
			builder.addPart("files", cbFile); 

			HttpEntity entity = builder.build();
			httpPost.setEntity(entity);

			try {
				HttpResponse response = httpclient.execute(targetHost,httpPost);
				entity = response.getEntity();
				resp = EntityUtils.toString(entity);

				Log.d("HTTP", "HTTP: OK");
				Log.d("response", resp);

				JSONObject jsonObj = new JSONObject(resp);

				if(jsonObj.getString("success").equals("true")){
					//successfully completed upload
					return params[1]+"-"+params[2];
				} else {
					Log.d("failure:", jsonObj.getString("reason"));
					return jsonObj.getString("reason");
				}
			} catch (ClientProtocolException e) {
				// TODO Auto-generated catch block
				Log.e("HTTP", "Error in http connection " + e.toString());
				return getString(R.string.server_not_reachable);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				Log.e("HTTP", "Error in http connection " + e.toString());
				return getString(R.string.server_not_reachable);
			} catch (JSONException e) {
				Log.e("JSON parsing", "Error in parsing JSON object: " + e.toString());
				return getString(R.string.server_not_reachable);
			}

		}

		@Override
		protected void onPostExecute(String result) {
			if(result.indexOf('-') > 0){ //success
				barProgressDialog.incrementProgressBy(1);
				if (barProgressDialog.getProgress() == barProgressDialog.getMax()) {
					barProgressDialog.dismiss();
				}

				//check 2 values passed as return value
				if(result.substring(0, result.indexOf('-')).equals(result.substring(result.indexOf('-')+1, result.length()))){
					error.setTextColor(Color.rgb(0, 126, 0));
					error.setText(getString(R.string.upload_success));
					Toast.makeText(HttpPostOperations.this, getString(R.string.upload_success), Toast.LENGTH_SHORT).show();
					finish();
				}
				DataSource.upload_file(_id);
			} else {
				barProgressDialog.dismiss();
				error.setTextColor(Color.RED);
				error.setText(result);
			}
		}

		@Override
		protected void onPreExecute() {}

		@Override
		protected void onProgressUpdate(Void... values) {}
	}

	private boolean readRememberMe(){
		if ( shared_pref.contains("cb_state") ){
			//ako je cb_state = true
			if( shared_pref.getBoolean("cb_state", false) ){
				username = shared_pref.getString("username", "");
				password = shared_pref.getString("password", "");
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}
	

	private void writeRememberMe(String un, String pw, boolean cb_state, String key){
		SharedPreferences.Editor editor = getSharedPreferences(getString(R.string.app_name), MODE_PRIVATE).edit();
		if( cb_state ){ //checkbox checked
			editor.putBoolean("cb_state", cb_state);
			editor.putString("key", key);
			editor.commit();
			if ( shared_pref.contains("username") )
			{
				username = shared_pref.getString("username", "");
			} else {
				editor.putString("username", un);
				editor.commit();
			}
			if ( shared_pref.contains("password") )
			{
				password = shared_pref.getString("password", "");
			} else {
				editor.putString("password", pw);
				editor.commit();
			}
		} else { //dont remember
			editor.putBoolean("cb_state", cb_state);
			editor.commit();
			editor.remove("username");
			editor.remove("password");
			editor.commit();
		}
	}

	private void startFileUpload(){

		//progress bar 4 showing upload status
		
		barProgressDialog.setTitle("Uploading Files ...");
		barProgressDialog.setMessage("Upload in progress ...");
		barProgressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
		barProgressDialog.setProgress(0);

		final File dir = new File(Environment.getExternalStorageDirectory()+"/"+
				getString(R.string.app_name)+"/"+
				session_id+"/");
		traverse(dir);
	}

	private void traverse (File dir) {
		if (dir.exists()) {
			File[] files = dir.listFiles();
			barProgressDialog.setMax(files.length);
			barProgressDialog.show();
			for (int i = 0; i < files.length; ++i) {
				File file = files[i];
				if (file.isDirectory()) {
					traverse(file);
				} else {
					new FileUploadOperation().execute(file.getName(), String.valueOf(i + 1), String.valueOf(files.length));
				}
			}
		}
	} 


	private boolean isNetworkConnected() {
		boolean isConnectedWifi = false;
		boolean isConnectedMobile = false;

		ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo[] netInfo = cm.getAllNetworkInfo();
		for (NetworkInfo ni : netInfo) {
			if (ni.getTypeName().equalsIgnoreCase("WIFI"))
				if (ni.isConnected())
					isConnectedWifi = true;
			if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
				if (ni.isConnected())
					isConnectedMobile = true;
		}
		return isConnectedWifi || isConnectedMobile;
	}

	protected boolean isInputCorrect(){

		String username = un.getText().toString();
		String password = pw.getText().toString();

		if( username.isEmpty() ){
			errorMsg.append(getString(R.string.un_missing_input)+"\n");
		}
		if( password.isEmpty() ){
			errorMsg.append(getString(R.string.pw_missing_input)+"\n");
		} /*else if ( password.length() < 8 ){
			errorMsg.append(getString(R.string.pw_too_short)+"\n");
		}*/

		if(errorMsg.length() == 0){
			return true;
		}

		return false;

	}
	
	@Override
    protected void onDestroy() {
		barProgressDialog.dismiss();
        super.onDestroy();
    }

}

