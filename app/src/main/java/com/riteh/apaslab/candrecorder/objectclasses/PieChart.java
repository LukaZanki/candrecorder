package com.riteh.apaslab.candrecorder.objectclasses;

import org.achartengine.ChartFactory;
import org.achartengine.GraphicalView;
import org.achartengine.model.CategorySeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public class PieChart {

    /** Colors to be used for the pie slices. */
    private static int[] COLORS = new int[] { 0xFF60BD68, 0xFF5DA5DA, 0xFFFAA43A, 0xFF4D4D4D,0xFFDECF3F };
    private final LinearLayout linearLayout;
    private Context context;
    /** The main series that will include all the data. */
    private CategorySeries mSeries =new CategorySeries("");
    /** The main renderer for the main dataset. */
    private DefaultRenderer mRenderer = new DefaultRenderer();
    /** The chart view that displays the data. */
    private GraphicalView mChartView;

    public PieChart(Context context, LinearLayout linearLayout,HashMap<String,Integer> data){
        this.linearLayout=linearLayout;
        this.context=context;
        mRenderer.setLabelsColor(0xFF989898);
        mRenderer.setZoomButtonsVisible(false);
        mRenderer.setLabelsTextSize(19);
        mRenderer.setDisplayValues(false);
        mRenderer.setZoomEnabled(false);
        mRenderer.setShowLegend(false);
        mRenderer.setInScroll(false);
        mRenderer.setStartAngle(270);
        mRenderer.setClickEnabled(false);
        mRenderer.setPanEnabled(false);
        generate(data);


    }
    private void generate(HashMap<String,Integer> data){
        Iterator it = data.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pair = (Map.Entry)it.next();
            if((int)pair.getValue()!=0) {
                mSeries.add("" + pair.getKey(), (int) pair.getValue());
                SimpleSeriesRenderer renderer = new SimpleSeriesRenderer();
                renderer.setColor(COLORS[(data.size() - 1) % COLORS.length]);
                mRenderer.addSeriesRenderer(renderer);
                it.remove(); // avoids a ConcurrentModificationException
            }
        }

        mChartView = ChartFactory.getPieChartView(context, mSeries, mRenderer);
        this.linearLayout.addView(mChartView, new LayoutParams(LayoutParams.MATCH_PARENT,
                LayoutParams.MATCH_PARENT));
        mChartView.repaint();

    }


}
